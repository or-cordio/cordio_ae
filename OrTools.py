import glob
from pathlib import Path
import numpy as np
from tqdm import tqdm
from progressbar import *
import pandas as pd
import seaborn as sns
import scipy as sp
# from ipapy.arpabetmapper import ARPABETMapper
from collections import Iterable
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as mdates

class MyTools:
    """
    class of general useful tools (methods)

       Cordio Medical - Confidential
         Version: 0.1    2020-07-23

         Revision History:
         |   Ver    | Author    | Date           | Change Description
         |----------|-----------|----------------|--------------------
         |   0.1    | Or        | 2020-07-23     | Initial
     """
    def getPhonemeNSentence_dicts(self, language, sentencePhonemeByLanguagePath):
        # ---Debug---
        # language = "Hebrew"
        # sentencePhonemeByLanguagePath = r"\\192.168.55.210\f$\Or-rnd\linguisticData\sentencePhonemeByLanguage.xlsx"
        # -----------
        sentencePhonemeByLanguagePath = Path(sentencePhonemeByLanguagePath)
        sentencePhonemeByLanguage_df = pd.read_excel(str(sentencePhonemeByLanguagePath), sheet_name=language, header=[2], dtype={'no.': str})
        sentencePhonemeByLanguage_df = sentencePhonemeByLanguage_df[["no.","IPA"]]
        sentencePhonemeByLanguage_df = sentencePhonemeByLanguage_df.dropna().reset_index().drop(["index"], axis=1)
        # zero padding:
        for i in range(len(sentencePhonemeByLanguage_df)):
            padLen = 4-len(sentencePhonemeByLanguage_df["no."][i])
            if padLen > 0:
                sentencePhonemeByLanguage_df["no."][i] = "".join(map(str,padLen*[0])) + sentencePhonemeByLanguage_df["no."][i]
        sentence2phonemByLang = dict(zip(sentencePhonemeByLanguage_df["no."], sentencePhonemeByLanguage_df["IPA"]))
        # create phonem2sentenceByLang:
        allIPA = []
        [allIPA.extend(IPA) for IPA in sentencePhonemeByLanguage_df["IPA"]]
        uniqueIPA = list(set(allIPA))
        # amapper = ARPABETMapper()
        # uniqueUnicode_IPA = [amapper.map_unicode_string(oneUniqueIPA, ignore=True)  for oneUniqueIPA in uniqueIPA]
        s2p_df = pd.DataFrame(columns=["IPA","sentence"])
        s2p_df["IPA"] = uniqueIPA
        for IPArowIdx in range(len(s2p_df["IPA"])):
            currSentenseList = []
            for sentencePhonemeByLanguage_df_Idx in range(len(sentencePhonemeByLanguage_df)):
                if s2p_df["IPA"][IPArowIdx] in sentencePhonemeByLanguage_df["IPA"][sentencePhonemeByLanguage_df_Idx]:
                    currSentenseList.append(sentencePhonemeByLanguage_df["no."][sentencePhonemeByLanguage_df_Idx])
            s2p_df["sentence"][IPArowIdx] = currSentenseList
        phonem2sentenceByLang = dict(zip(s2p_df["IPA"], s2p_df["sentence"]))

        return sentence2phonemByLang, phonem2sentenceByLang



    def flatten(self, lis):
        """
        convert a nested list into a one dimensional list

        Works for any level of nesting:
        a = [1,[2,2,[2]],4]
        list(flatten(a))
        out:
        [1, 2, 2, 2, 4]
        """
        for item in lis:
            if isinstance(item, Iterable) and not isinstance(item, str):
                for x in self.flatten(item):
                    yield x
            else:
                yield item

    def flatten_list(self, lis):
        return list(self.flatten(lis))

    def getDBPatientFiles(self,DB_root, file_type):
        allHMOs = next(os.walk(DB_root))[1]
        allHMOs = [hmo for hmo in allHMOs if len(hmo) == 3 and hmo.isupper()]
        allPatientsPaths = []
        for hmo in allHMOs:
            currHMOPatients = next(os.walk(DB_root / hmo))[1]
            allHMOPatientsPaths = [DB_root / hmo / currHMOPatient for currHMOPatient in currHMOPatients]
            allPatientsPaths.extend(allHMOPatientsPaths)

        # patientsPaths = [localRootPath+"\\"+patient[0:3]+"\\"+patient for patient in patients]
        asrJasonNPklPaths = []
        [asrJasonNPklPaths.extend(glob.glob(str(patientPath) + "//*"+file_type, recursive=False)) for patientPath in allPatientsPaths]

        return asrJasonNPklPaths

    def uniqueListInOrder(self, list):
        uqn = []
        for n in list:
            if n not in uqn:
                uqn.append(n)
        else:
            pass
        return uqn

    def MyPlot(self, y, x=None, title=None, x_labels=None, y_labels=None, showMin=False, saveURL=None,labels=None, xLabels = None):
        plt.figure(dpi=200)
        if title is not None: plt.title(title)
        if x_labels is not None: plt.xlabel(x_labels)
        if y_labels is not None: plt.ylabel(y_labels)
        plt.grid()

        if showMin:
            ymin = min(y)
            xpos = y.index(ymin)
            xmax = list(range(len(y)))[xpos]
            plt.annotate(str(ymin), xy=(xmax, ymin), xytext=(xmax - 0.1 * xmax, ymin + 0.1 * ymin),
                         arrowprops=dict(facecolor='steelblue', shrink=0.05), color='steelblue'
                         )
        if x is None: x = list(range(len(y)))
        if labels is not None and len(labels)==len(x):
            uniqeLabels = self.uniqueListInOrder(labels)
            ymin = min(y)
            lastMax_xLabel = 0
            for label in list(uniqeLabels):
                plt.plot(list(range(lastMax_xLabel, lastMax_xLabel + labels.count(label))), [ymin-0.5*ymin]*labels.count(label), color=np.random.rand(3,))
                plt.annotate(label, (np.mean(list(range(lastMax_xLabel, lastMax_xLabel + labels.count(label)))), ymin), textcoords="offset points", xytext=(0, 3), ha='center')
                lastMax_xLabel += labels.count(label)
        # strx = [pd.to_datetime(str(xi)) for xi in x]
        # df = pd.DataFrame(y,index=x)
        # if type(x[0]).__name__ == 'datetime':
        #     x_values = [datetime.datetime.strptime(d, "%Y-%m-%d %H:%M:%S").date() for d in strx]
        #     ax = plt.gca()
        #     formatter = mdates.DateFormatter("%d-%m-%Y")
        #     ax.xaxis.set_major_formatter(formatter)
        #     locator = mdates.DayLocator()
        #     ax.xaxis.set_major_locator(locator)
            # plt.plot(x_values, y_values)
        # df.plot()
        plt.plot(x, y)
        if saveURL is not None:
            fig = plt.gcf()
            fig.savefig(saveURL)
            plt.show()
            fig.savefig(saveURL)
        else:
            plt.show()

            # plt.savefig(saveURL+".png")
        plt.close()