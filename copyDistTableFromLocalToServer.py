import os
import subprocess
from distutils.dir_util import copy_tree
from pathlib import Path
from tqdm import trange

localDB = r"/home/itai/work/db_run_detector"
# localDB = r"D:\DB"
localDB = Path(localDB)
remoteDB = "/db"
remoteDB = Path(remoteDB)
HMO_filter = "BLN"

HMOs_dirs = next(os.walk(str(localDB)))[1]
HMOs_paths = [localDB / hmo for hmo in HMOs_dirs if len(hmo)==3]

#filter data
if HMO_filter != None:
    HMOs_paths = [HMO_path for HMO_path in HMOs_paths if HMO_filter in str(HMO_path)]

print("HMOs directories to copy from:\n")
print(str(HMOs_paths))

for HMO_path in HMOs_paths:
    allHMOPatients = next(os.walk(str(HMO_path)))[1]
    allHMOPatients_paths = [HMO_path / patient for patient in allHMOPatients]
    # progressbar:
    t = trange(len(allHMOPatients_paths), desc='Bar desc', leave=True)
    for HMOPatient_path, i in zip(allHMOPatients_paths, t):
        sourceDistTableDir = HMOPatient_path / "results" / "CordioPythonDistanceCalculation"
        destDistTableDir = remoteDB / HMO_path.name / HMOPatient_path.name /  "results" / "CordioPythonDistanceCalculation"
        # print("copy "+str(sourceDistTableDir)+" to "+str(destDistTableDir)+" ...")
        # progressbar
        t.set_description("copy %s ..." % str(sourceDistTableDir))
        t.refresh()
        # copy
        try:
            bashCommand = "cp -r "+str(sourceDistTableDir)+"/. /run/user/1000/gvfs/smb-share\:server\=192.168.55.210\,share\=f\$"+str(destDistTableDir)
            #process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
            #output, error = process.communicate()
            os.system(bashCommand)
            # copy_tree(str(sourceDistTableDir), str(destDistTableDir))
        except:
            print("\nfailed coping "+str(sourceDistTableDir))
        # print("copy succeeded")




