# Define AutoEncoder Neural Network:
# -----------------------
import math
import time

import torch.nn as nn
import torch.nn.functional as F
import torch
from pathlib import Path
import warnings

def AE_Net_manual_weight_init(m, seed=786):
    if isinstance(m, nn.Conv2d):
        torch.manual_seed(seed)
        torch.nn.init.xavier_uniform_(m.weight.data)
        torch.nn.init.zeros_(m.bias.data)
    if isinstance(m, nn.Linear):
        torch.manual_seed(seed)
        torch.nn.init.xavier_uniform_(m.weight.data)
        torch.nn.init.zeros_(m.bias.data)
    if isinstance(m, nn.ConvTranspose2d):
        torch.manual_seed(seed)
        torch.nn.init.xavier_uniform_(m.weight.data)
        torch.nn.init.zeros_(m.bias.data)

class AE_Net(nn.Module):
    def __init__(self):
        super(AE_Net, self).__init__()
        # self.conv1 = nn.Conv2d(1,3,3) #torch.nn.Conv2d(in_channels: int, out_channels: int, kernel_size: Union[T, Tuple[T, T]])
        # self.pool = nn.MaxPool2d(2, 2, return_indices=True)
        # self.conv2 = nn.Conv2d(3, 3, 2)
        # self.fc1 = nn.Linear(1*3*3*2, 15)
        # self.fc2 = nn.Linear(15, 12)
        # self.fc3 = nn.Linear(12, 10)
        # self.fc4 = nn.Linear(10, 9)
        # self.fc5 = nn.Linear(9, 10)
        # self.fc6 = nn.Linear(10, 12)
        # self.fc7 = nn.Linear(12, 15)
        # self.fc8 = nn.Linear(15, 18)
        # self.convTranspose2 = nn.ConvTranspose2d(3, 3, 2)
        # self.UnPool = nn.MaxUnpool2d(2, 2)
        # self.convTranspose1 = nn.ConvTranspose2d(3, 1, 3)

        # self.conv1 = nn.Conv2d(1, 3, 3)  # torch.nn.Conv2d(in_channels: int, out_channels: int, kernel_size: Union[T, Tuple[T, T]])
        # self.pool = nn.MaxPool2d(2, 2, return_indices=True)
        # self.conv2 = nn.Conv2d(3, 3, 2)
        # self.fc1 = nn.Linear(3*2*4, 20)
        # self.fc2 = nn.Linear(20, 18)
        # self.fc3 = nn.Linear(18, 15)
        # self.fc4 = nn.Linear(15, 12)
        # self.fc5 = nn.Linear(12, 10)
        # self.fc6 = nn.Linear(10, 9)
        # self.fc7 = nn.Linear(9, 10)
        # self.fc8 = nn.Linear(10, 12)
        # self.fc9 = nn.Linear(12, 15)
        # self.fc10 = nn.Linear(15, 18)
        # self.fc11 = nn.Linear(18, 20)
        # self.fc12 = nn.Linear(20, 24)
        # self.convTranspose2 = nn.ConvTranspose2d(3, 3, 2)
        # self.UnPool = nn.MaxUnpool2d(2, 2)
        # self.convTranspose1 = nn.ConvTranspose2d(3, 1, 3)

        # for input size: torch.Size([7, 3, 18, 2])
        # TODO: Or: creat fully connencted with dropout
        self.conv1 = nn.Conv2d(1, 3, 3)  # torch.nn.Conv2d(in_channels: int, out_channels: int, kernel_size: Union[T, Tuple[T, T]])
        self.pool = nn.MaxPool2d(2, 2, return_indices=True)
        self.conv2 = nn.Conv2d(3, 3, 2)
        self.fc1 = nn.Linear(3*22*2, 100)
        self.fc2 = nn.Linear(100, 80)
        # dropout
        self.fc3 = nn.Linear(80, 70)
        self.fc4 = nn.Linear(70, 60)
        self.fc5 = nn.Linear(60, 50)
        self.fc6 = nn.Linear(50, 40)
        # dropout
        self.fc7 = nn.Linear(40, 35)
        self.fc8 = nn.Linear(35, 30)
        self.fc9 = nn.Linear(30, 25)
        self.fc10 = nn.Linear(25, 20)
        self.fc11 = nn.Linear(20, 18)


        self.fc12 = nn.Linear(18, 20)
        self.fc13 = nn.Linear(20, 25)
        self.fc14 = nn.Linear(25, 30)
        self.fc15 = nn.Linear(30, 35)
        self.fc16 = nn.Linear(35, 40)
        # dropout
        self.fc17 = nn.Linear(40, 50)
        self.fc18 = nn.Linear(50, 60)
        self.fc19 = nn.Linear(60, 70)
        self.fc20 = nn.Linear(70, 80)
        # dropout
        self.fc21 = nn.Linear(80, 100)
        self.fc22 = nn.Linear(100, 132)
        self.convTranspose2 = nn.ConvTranspose2d(3, 3, 2)
        self.UnPool = nn.MaxUnpool2d(2, 2)
        self.convTranspose1 = nn.ConvTranspose2d(3, 1, 3)

        # Define proportion or neurons to dropout
        self.dropout = nn.Dropout(0.25)

    def manual_weight_init(self, m, seed=786):
        if isinstance(m, nn.Conv2d):
            torch.manual_seed(seed)
            torch.nn.init.xavier_uniform_(m.weight.data)
            torch.nn.init.zeros_(m.bias.data)
        if isinstance(m, nn.Linear):
            torch.manual_seed(seed)
            torch.nn.init.xavier_uniform_(m.weight.data)
            torch.nn.init.zeros_(m.bias.data)
        if isinstance(m, nn.ConvTranspose2d):
            torch.manual_seed(seed)
            torch.nn.init.xavier_uniform_(m.weight.data)
            torch.nn.init.zeros_(m.bias.data)

    @classmethod
    def my_name(cls_):
        return cls_.__name__

    def class_name(self):
        return self.__class__.__name__

    def forward(self, x):
        # # x input size: [284, 1, 16, 12] [batch_size, channels, height, width]
        # size1 = F.relu(self.conv1(x)).size()
        # x, indices1  = self.pool(F.relu(self.conv1(x)))
        # size2 = F.relu(self.conv2(x)).size()
        # x, indices2 = self.pool(F.relu(self.conv2(x))) # input x size: [1, 3, 7, 5]
        # savedShape = x.shape
        #
        # x = x.view(-1, self.num_flat_features(x)) # flatten x from 2d to 1d, input x size: [1, 3, 3, 2]
        # x = F.relu(self.fc1(x)) # input x size: [1, 24]
        # x = F.relu(self.fc2(x)) # input x size: [1, 20]
        # x = F.relu(self.fc3(x)) # input x size: [1, 18]
        # x = F.relu(self.fc4(x)) # input x size: [1, 12]
        # x = F.relu(self.fc5(x)) # input x size: [1, 6]
        # x = F.relu(self.fc6(x)) # input x size: [1, 2]
        # x = F.relu(self.fc7(x)) # input x size: [1, 6]
        # x = F.relu(self.fc8(x)) # input x size: [1, 12]
        # x = F.relu(self.fc9(x)) # input x size: [1, 12]
        # x = F.relu(self.fc10(x)) # input x size: [1, 12]
        # x = F.relu(self.fc11(x)) # input x size: [1, 12]
        # x = F.relu(self.fc12(x)) # input x size: [1, 12]
        # x = x.view(savedShape) # input x size: [1, 18]
        #
        # x = self.UnPool(x, indices2,output_size=size2) # input x size: [1, 3, 3, 2]
        # x = self.convTranspose2(F.relu(x)) # input x size: [1, 3, 6, 4]
        # x = self.UnPool(x, indices1,output_size=size1) # input x size: [1, 3, 7, 5]
        # x = self.convTranspose1(F.relu(x))  # input x size: [1, 3, 14, 10]
        # return x # input x size: [1, 1, 16, 12]

        # x input size: [284, 1, 16, 12] [batch_size, channels, height, width]
        size1 = F.relu(self.conv1(x)).size()
        x, indices1 = self.pool(F.relu(self.conv1(x)))
        size2 = F.relu(self.conv2(x)).size()
        x, indices2 = self.pool(F.relu(self.conv2(x)))  # input x size: [1, 3, 7, 5]
        savedShape = x.shape

        x = x.view(-1, self.num_flat_features(x))  # flatten x from 2d to 1d, input x size: [1, 3, 3, 2]
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.dropout(x)
        x = F.relu(self.fc3(x))
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))
        x = F.relu(self.fc6(x))
        x = self.dropout(x)
        x = F.relu(self.fc7(x))
        x = F.relu(self.fc8(x))
        x = F.relu(self.fc9(x))
        x = F.relu(self.fc10(x))
        x = F.relu(self.fc11(x))


        x = F.relu(self.fc12(x))
        x = F.relu(self.fc13(x))
        x = F.relu(self.fc14(x))
        x = F.relu(self.fc15(x))
        x = F.relu(self.fc16(x))
        x = self.dropout(x)
        x = F.relu(self.fc17(x))
        x = F.relu(self.fc18(x))
        x = F.relu(self.fc19(x))
        x = F.relu(self.fc20(x))
        x = self.dropout(x)
        x = F.relu(self.fc21(x))
        x = F.relu(self.fc22(x))
        x = x.view(savedShape)

        x = self.UnPool(x, indices2, output_size=size2)  # input x size: [1, 3, 3, 2]
        x = self.convTranspose2(F.relu(x))  # input x size: [1, 3, 6, 4]
        x = self.UnPool(x, indices1, output_size=size1)  # input x size: [1, 3, 7, 5]
        x = self.convTranspose1(F.relu(x))  # input x size: [1, 3, 14, 10]
        return x  # input x size: [1, 1, 16, 12]


    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

class AE_deepNet(nn.Module):
    def __init__(self):
        super(AE_deepNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 3, 3)  # torch.nn.Conv2d(in_channels: int, out_channels: int, kernel_size: Union[T, Tuple[T, T]])
        self.pool = nn.MaxPool2d(2, 2, return_indices=True)
        self.conv2 = nn.Conv2d(3, 3, 2)
        self.fc1 = nn.Linear(1 * 3 * 3 * 2, 16)
        self.fc2 = nn.Linear(16,14)
        self.fc3 = nn.Linear(14, 12)
        self.fc4 = nn.Linear(12, 8)
        self.fc5 = nn.Linear(8, 6)
        self.fc6 = nn.Linear(6, 3)
        self.fc7 = nn.Linear(3, 6)
        self.fc8 = nn.Linear(6, 8)
        self.fc9 = nn.Linear(8, 12)
        self.fc10 = nn.Linear(12, 14)
        self.fc11 = nn.Linear(14, 16)
        self.fc12 = nn.Linear(16, 18)
        self.convTranspose2 = nn.ConvTranspose2d(3, 3, 2)
        self.UnPool = nn.MaxUnpool2d(2, 2)
        self.convTranspose1 = nn.ConvTranspose2d(3, 1, 3)

    @classmethod
    def my_name(cls_):
        return cls_.__name__

    def class_name(self):
        return self.__class__.__name__

    def forward(self, x):
        # x input size: [1, 1, 16, 12]
        size1 = F.relu(self.conv1(x)).size()
        x, indices1 = self.pool(F.relu(self.conv1(x)))
        size2 = F.relu(self.conv2(x)).size()
        x, indices2 = self.pool(F.relu(self.conv2(x)))  # input x size: [1, 3, 7, 5]
        savedShape = x.shape

        x = x.view(-1, self.num_flat_features(x))  # flatten x from 2d to 1d, input x size: [1, 3, 3, 2]
        x = F.relu(self.fc1(x))  # input x size: [1, 18]
        x = F.relu(self.fc2(x))  # input x size: [1, 12]
        x = F.relu(self.fc3(x))  # input x size: [1, 6]
        x = F.relu(self.fc4(x))  # input x size: [1, 2]
        x = F.relu(self.fc5(x))  # input x size: [1, 6]
        x = F.relu(self.fc6(x))  # input x size: [1, 12]
        x = F.relu(self.fc7(x))  # input x size: [1, 12]
        x = F.relu(self.fc8(x))  # input x size: [1, 12]
        x = F.relu(self.fc9(x))  # input x size: [1, 12]
        x = F.relu(self.fc10(x))  # input x size: [1, 12]
        x = F.relu(self.fc11(x))  # input x size: [1, 12]
        x = F.relu(self.fc12(x))  # input x size: [1, 12]
        x = x.view(savedShape)  # input x size: [1, 18]

        x = self.UnPool(x, indices2, output_size=size2)  # input x size: [1, 3, 3, 2]
        x = self.convTranspose2(F.relu(x))  # input x size: [1, 3, 6, 4]
        x = self.UnPool(x, indices1, output_size=size1)  # input x size: [1, 3, 7, 5]
        x = self.convTranspose1(F.relu(x))  # input x size: [1, 3, 14, 10]
        return x  # input x size: [1, 1, 16, 12]

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

class AE_FC_Net(nn.Module):
    def __init__(self):
        super(AE_FC_Net, self).__init__()
        self.fc1 = nn.Linear(216, 192)
        self.fc2 = nn.Linear(192, 144)
        self.fc3 = nn.Linear(144, 126)
        self.fc4 = nn.Linear(126, 108)
        self.fc5 = nn.Linear(108, 90)
        self.fc6 = nn.Linear(90, 70)
        self.fc7 = nn.Linear(70, 60)
        self.fc8 = nn.Linear(60, 54)
        self.fc9 = nn.Linear(54, 60)
        self.fc10 = nn.Linear(60, 70)
        self.fc11 = nn.Linear(70, 90)
        self.fc12 = nn.Linear(90, 108)
        self.fc13 = nn.Linear(108, 126)
        self.fc14 = nn.Linear(126, 144)
        self.fc15 = nn.Linear(144, 192)
        self.fc16 = nn.Linear(192, 216)

    @classmethod
    def my_name(cls_):
        return cls_.__name__

    def class_name(self):
        return self.__class__.__name__

    def forward(self, x):
        x = F.relu(self.fc1(x))  # input x size: [1, 18]
        x = F.relu(self.fc2(x))  # input x size: [1, 12]
        x = F.relu(self.fc3(x))  # input x size: [1, 6]
        x = F.relu(self.fc4(x))  # input x size: [1, 2]
        x = F.relu(self.fc5(x))  # input x size: [1, 6]
        x = F.relu(self.fc6(x))  # input x size: [1, 12]
        x = F.relu(self.fc7(x))  # input x size: [1, 12]
        x = F.relu(self.fc8(x))  # input x size: [1, 12]
        x = F.relu(self.fc9(x))  # input x size: [1, 12]
        x = F.relu(self.fc10(x))  # input x size: [1, 12]
        x = F.relu(self.fc11(x))  # input x size: [1, 12]
        x = F.relu(self.fc12(x))  # input x size: [1, 12]
        x = F.relu(self.fc13(x))  # input x size: [1, 12]
        x = F.relu(self.fc14(x))  # input x size: [1, 12]
        x = F.relu(self.fc15(x))  # input x size: [1, 12]
        x = F.relu(self.fc16(x))  # input x size: [1, 12]
        return x  # input x size: [1, 1, 16, 12]

def AE_train(epochs, train_loader, chosenPhnLen, featureFrame_size, device, model, optimizer, criterion,
             save_location_path=r'\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump',
             modelName='unnamedMOdel', toleranceParcent=10, toleranceLen: int=10):
    tolerance = toleranceParcent/100
    lossList = []
    t = round(time.time())
    for epoch in range(epochs):
        loss = 0
        # optimizer.zero_grad()
        for batch_ndx, batch_features in enumerate(train_loader):
            # move data to cuda if needed:
            if next(model.parameters()).is_cuda:
                batch_features = batch_features.to('cuda', non_blocking=True)
            # reshape mini-batch data to [N, 784] matrix
            # load it to the active device
            # if type(model).__name__ != 'AE_FC_Net':
            #     batch_features = batch_features.reshape(-1, 1, chosenPhnLen, featureFrame_size).to(device)
            # numOfRows = int(batch_features.shape[0] / 12)  # mfcc frame feature length = 12
            # sample = torch.reshape(batch_features, (1, 1, numOfRows, 12))

            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer.zero_grad()

            # compute reconstructions
            outputs = model(batch_features)

            # compute training reconstruction loss
            train_loss = criterion(outputs, batch_features)

            # compute accumulated gradients
            train_loss.backward()

            # perform parameter update based on current gradients
            optimizer.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss.item()

        # compute the epoch training loss
        loss = loss / len(train_loader)
        lossList.append(loss)
        elapsed = round(time.time()) - t
        elapsedS = elapsed % 60
        _elapsedM = math.floor(elapsed / 60)
        elapsedM = math.floor(elapsed / 60) % 60
        elapsedH = math.floor(_elapsedM / 60)

        # display the epoch training loss
        print("epoch: " + str(epoch+1) + "/" + str(epochs) +
              ", loss: " + str(loss)[0:8] +
              ", elapsed: " + str(elapsedH) + ":" + str(elapsedM) + ":" + str(elapsedS))
        # print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, epochs, loss))

        # training efficiency control:
        if len(lossList) >= toleranceLen:
            stopRunFlag = True
            # check if loss chnges (not staying with about the same value):
            for i in range(1, toleranceLen+1):
                if not ((lossList[-toleranceLen]<=lossList[-i]+tolerance*lossList[-toleranceLen]) and
                        ((lossList[-i]-tolerance*lossList[-toleranceLen]<=lossList[-toleranceLen]))):
                    stopRunFlag = False
                    break
            # check if loss doesnt go up:
            if toleranceLen <= len(lossList):
                checkSum = 0
                for i in range(-toleranceLen, -1):
                    if lossList[i] < lossList[i+1]:
                        # print(str(lossList[i]) + "<" + str(lossList[i+1]))
                        checkSum += 1
                    # else:
                        # print(str(lossList[i]) + ">" + str(lossList[i + 1]))
                if checkSum != (toleranceLen-1): stopRunFlag = False


        if ("stopRunFlag" in locals()) and stopRunFlag:
            break



    torch.save(model.state_dict(), str(save_location_path / Path(modelName)))
    listName = str(Path(modelName).stem) + '_Losslist.pt'
    torch.save(lossList, str(save_location_path / Path(listName)))

    return model, lossList

def AE_test(model, test_loader, chosenPhnLen, featureFrame_size, device, criterion):
    loss = []
    for batch_ndx, batch_features in enumerate(test_loader):
        # reshape mini-batch data to [N, 784] matrix
        # load it to the active device
        batch_features = batch_features.to(device)
        # batch_features = batch_features.reshape(-1, 1, chosenPhnLen, featureFrame_size).to(device)
        # make sure input is in model input size:
        if (chosenPhnLen != batch_features.size()[-2]):
            # raise ValueError("ValueError exception thrown")
            warnings.warn("data chosen phoneme length is different from model input length, narrow data")
            batch_features = torch.narrow(input=batch_features, dim=2, start=0, length=chosenPhnLen)

        # compute reconstructions
        outputs = model(batch_features)

        # compute training reconstruction loss
        train_loss = criterion(outputs, batch_features)

        # add the mini-batch training loss to epoch loss
        loss.append(train_loss.item())

    return loss

