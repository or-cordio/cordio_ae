import warnings

import torch
import math
import matplotlib.pyplot as plt

from torch import optim, nn, cat
from pathlib import Path
from CordioAutoEncoder import AE_FC_Net, AE_train, AE_test
from CordioAE_preprocessData import preprocessData_singleTriphon

# from Epochsviz.epochsviz import Epochsviz

# warnings.filterwarnings('ignore')
# with warnings.catch_warnings():
warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')

# initializing objects:
# --------------------
#  use gpu if available
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")
# create a model from `AE` autoencoder class
# load it to the specified device, either gpu or cpu
model = AE_FC_Net().to(device)
# create an optimizer object
# Adam optimizer with learning rate 1e-3
optimizer = optim.Adam(model.parameters(), lr=1e-2)
# mean-squared error loss
criterion = nn.MSELoss()

# visualization:
# eviz = Epochsviz()

# getting data ready:
# ------------------
patient = "BLN-0021"#"BLN-0001"
speechEvent = "i"
dbPath = r"\\192.168.55.210\db"
saveLocationUrl = r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump"
saveName = patient+"_"+speechEvent+"_mfcc_framesBySe_df"
jobsNum = 20
forceReCulc = False
feature="mfcc"
# trianBatch_size = 155
if feature=="mfcc": featureFrame_size = 12
else: featureFrame_size = 1


fullData = preprocessData_singleTriphon(patient=patient, speechEvent=speechEvent, dbPath=dbPath, saveLocationUrl=saveLocationUrl,
                                        saveName=saveName, jobsNum=jobsNum, forceReCulc=forceReCulc, feature=feature)
all_dry_data = fullData.FeatureFrames_dry_tensor
all_wet_data = fullData.FeatureFrames_wet_tensor
all_unknown_data = fullData.FeatureFrames_unknown_tensor
# rowShaffledFeatureFrames_tensor = fullData.FeatureFrames_tensor[torch.randperm(fullData.FeatureFrames_tensor.size()[0])] # each row describes the features of a single audio file speech event
train_dataset = all_dry_data[0:math.floor(len(all_dry_data) * 0.9)].clone()
test_dataset = all_dry_data[math.ceil(len(all_dry_data) * 0.9):].clone()
test_dataset = cat((train_dataset, test_dataset, all_unknown_data, all_wet_data))

trianBatch_size = int(train_dataset.size()[1]/featureFrame_size)
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=18, shuffle=True, num_workers=4, pin_memory=True)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=18, shuffle=False, num_workers=4)


# fullData.get_all_speech_event_features_for_patient(patient_folder_path_db=dbPath+"\\"+patient[0:3]+"\\"+patient, speech_event=speechEvent)

# training:
# --------
if __name__ == '__main__':

    save_location_path = Path(r'\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump')
    model_name = 'trainedAE_FCNetModel_batchIs_18.pickle'

    # with warnings.catch_warnings():
    # warnings.filterwarnings('ignore', r'All-NaN axis encountered maxCS = np.nanmax(clinicalStatus_dt) if clinicalStatus_dt else np.nan')
    warnings.filterwarnings('ignore')

    epochs = 500

    if not Path.is_file(save_location_path / model_name):
        model, lossList = AE_train(epochs, train_loader, fullData, featureFrame_size, device, model, optimizer, criterion, save_location_path=r'\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump', modelName=model_name)
        plt.plot(range(len(lossList)),lossList)
        plt.show()

    # testing:
    model.load_state_dict(torch.load(save_location_path / model_name))
    model = torch.load(save_location_path / model_name).to(device)
    testLoss = AE_test(model, test_loader, fullData, featureFrame_size, device, criterion)








