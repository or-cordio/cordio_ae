import os
import subprocess
from distutils.dir_util import copy_tree
from pathlib import Path
from tqdm import trange

# localDB = r"/home/itai/work/db_run_detector"
localDB = r"/home/ubuntu/db"
localDB = Path(localDB)
remoteDB = r"/db"
remoteDB = Path(remoteDB)
HMO_filter = "HYA"

HMOs_dirs = next(os.walk(str(localDB)))[1]
HMOs_paths = [localDB / hmo for hmo in HMOs_dirs if len(hmo)==3]

#filter data
if HMO_filter != None:
    HMOs_paths = [HMO_path for HMO_path in HMOs_paths if HMO_filter in str(HMO_path)]

print("HMOs directories to copy from:\n")
[print(str(HMO_path)) for HMO_path in HMOs_paths]

for HMO_path in HMOs_paths:
    allHMOPatients = next(os.walk(str(HMO_path)))[1]
    allHMOPatients_paths = [HMO_path / patient for patient in allHMOPatients]
    # progressbar:
    t = trange(len(allHMOPatients_paths)-1, desc='Bar desc', leave=True)
    for HMOPatient_path, i in zip(allHMOPatients_paths, t):
        sourceDistTableDir = HMOPatient_path / "results" / "CordioPythonDistanceCalculation"
        destDistTableDir = remoteDB / HMO_path.name / HMOPatient_path.name /  "results" / "CordioPythonDistanceCalculation"
        # print("copy "+str(sourceDistTableDir)+" to "+str(destDistTableDir)+" ...")
        # progressbar
        t.set_description("copy %s ..." % str(sourceDistTableDir))
        t.refresh()
        # copy
        try:
            if os.path.isdir(str(sourceDistTableDir)):
                bashCommand = "aws s3 sync "+str(sourceDistTableDir)+"/. s3://cordio-research-test/uploadToServer"+str(destDistTableDir)+" --no-progress --only-show-errors"
                os.system(bashCommand)
        except:
            print("\nfailed coping "+str(sourceDistTableDir))
        # print("copy succeeded")