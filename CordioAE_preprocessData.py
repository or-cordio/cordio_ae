# sys.path.append("D:\work\code\Python\CordioAlgorithmsPython")
# sys.path.append(r"D:\work\code\Python\CordioAlgorithmsPython\infrastructure")

# import sys
# sys.path.insert(0,r"D:\work\code\Python\CordioAlgorithmsPython\infrastructure")
import glob
from pathlib import Path
import numpy as np
from tqdm import tqdm
from progressbar import *
import pandas as pd
import seaborn as sns
import scipy as sp
from OrTools import MyTools
from joblib import Parallel, delayed
import torch
import pickle
import warnings
from collections import ChainMap
from infrastructure.patientsInformation.CordioPatientClinicalInformation import CordioClinicalInformation
clinicalInformation = CordioClinicalInformation
from infrastructure.CordioRecordingLinks import ProcessDirectory
from infrastructure.CordioRecordingLinks import ProcessDirectorySentence
from infrastructure.CordioRecordingsWithFeatures import RecordingFeatures
from infrastructure.patientsInformation.CordioPatientClinicalInformation import CordioClinicalInformation
from infrastructure.CordioFile import CordioFile
from utils.GeneralTools import Tools

class preprocessData_singleTriphon:

    # defined variables for class:
    # DATETIME_FORMAT = "%d/%m/%Y_%H:%M"
    # CLINICAL_STATUS_DRY = 0.0
    # CLINICAL_STATUS_UNKNOWN = 0.5
    # CLINICAL_STATUS_WET = 1.0
    # ClinicalInfoDict = {CLINICAL_STATUS_DRY: "dry",
    #                     CLINICAL_STATUS_UNKNOWN: "unknown",
    #                     CLINICAL_STATUS_WET: "wet"}


    def __init__(self, patient, speechEvent, dbPath=r"\\192.168.55.210\db",
                 saveLocationUrl=r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump", saveNameStart=None, jobsNum=1,
                 forceReCulc=False, feature="mfcc", se_triphon=None, disablePrints=False, turnOffProgBars=False):

        self.NO_CLINICAL_STATUS = -1.0
        self.CLINICAL_STATUS_DRY = 0.0
        self.CLINICAL_STATUS_UNKNOWN = 0.5
        self.CLINICAL_STATUS_WET = 1.0
        self.ClinicalInfoDict = {self.CLINICAL_STATUS_DRY: "dry",
                                 self.CLINICAL_STATUS_UNKNOWN: "unknown",
                                 self.CLINICAL_STATUS_WET: "wet",
                                 self.NO_CLINICAL_STATUS: "patient was not in the experiment in this date"}


        self.patient = patient
        self.hospital = patient[0:3]
        self.feature = feature
        self.patient_folder_path_db = os.path.join(dbPath, self.hospital+"\\"+self.patient)
        self.speechEvent = speechEvent
        self.dbPath = Path(dbPath)
        self.saveLocationUrl = Path(saveLocationUrl)
        # self.saveName = saveName
        self.jobsNum = jobsNum
        self.allAsrPaths = glob.glob(str(self.patient_folder_path_db) + "//*asr.json", recursive=False)
        self.forceReCulc = forceReCulc
        if (se_triphon[0]=="/" and  se_triphon[-1]=="/"): self.se_triphon = se_triphon[1:-1]
        else: self.se_triphon = se_triphon
        self.disablePrints = disablePrints
        self.turnOffProgBars = turnOffProgBars

        if saveNameStart is None:
            if len(self.speechEvent_id) > 7: str_SEid = str(self.speechEvent_id[0:7])[0:-1] + ", more_]"
            else: str_SEid = str(self.speechEvent_id)
            self.saveNameStart = self.patient + '_' + self.feature + '_' + str_SEid
        else:
            self.saveNameStart = saveNameStart

        self.FeatureFramesDates_wet_List = None
        self.FeatureFramesDates_dry_List = None
        self.FeatureFramesDates_unknown_List = None
        self.__NoneUniformLenFeature_framesBySe_df = None
        self.__FeatureFrames_wet_tensor = None
        self.__FeatureFrames_dry_tensor = None
        self.__FeatureFrames_unknown_tensor = None
        self.__chosenPhnLen = None
        self.__FeatureFrames_df = None
        self.__FeatureFrames_List = None
        self.__FeatureFrames_wet_List = None
        self.__FeatureFrames_dry_List = None
        self.__FeatureFrames_unknown_List = None
        self.__FeatureFrames_tensor = None

    @property
    def NoneUniformLenFeature_framesBySe_df(self):
        if self.__NoneUniformLenFeature_framesBySe_df is None:
            self.__NoneUniformLenFeature_framesBySe_df = self.getNoneUniformLenFeatureFrames_df(self.forceReCulc, self.feature)
        return self.__NoneUniformLenFeature_framesBySe_df

    @property
    def FeatureFrames_df(self):
        if self.__FeatureFrames_df is None:
            self.__FeatureFrames_df = self.getFeatureFrames_df(self.forceReCulc, self.feature)
        return self.__FeatureFrames_df

    @property
    def chosenPhnLen(self):
        if self.__chosenPhnLen is None:
            self.__chosenPhnLen = self.getInpunLength(self.NoneUniformLenFeature_framesBySe_df, self.feature)
        return self.__chosenPhnLen

    # FeatureFrames_Lists:
    @property
    def FeatureFrames_List(self):
        if self.__FeatureFrames_List is None:
            self.__FeatureFrames_List = self.__FeatureFrames_dfToList__(self.FeatureFrames_df, clinicaLabel="None")
        return self.__FeatureFrames_List

    @property
    def FeatureFrames_wet_List(self):
        if self.__FeatureFrames_wet_List is None:
            self.__FeatureFrames_wet_List, self.FeatureFramesDates_wet_List = self.__FeatureFrames_dfToList__(self.FeatureFrames_df, clinicaLabel="wet")
        return self.__FeatureFrames_wet_List

    @property
    def FeatureFrames_dry_List(self):
        if self.__FeatureFrames_dry_List is None:
            self.__FeatureFrames_dry_List, self.FeatureFramesDates_dry_List  = self.__FeatureFrames_dfToList__(self.FeatureFrames_df, clinicaLabel="dry")
        return self.__FeatureFrames_dry_List

    @property
    def FeatureFrames_unknown_List(self):
        if self.__FeatureFrames_unknown_List is None:
            self.__FeatureFrames_unknown_List, self.FeatureFramesDates_unknown_List = self.__FeatureFrames_dfToList__(self.FeatureFrames_df, clinicaLabel="unknown")
        return self.__FeatureFrames_unknown_List

    # FeatureFrames_tensors:
    @property
    def FeatureFrames_tensor(self):
        if self.__FeatureFrames_tensor is None:
            self.__FeatureFrames_tensor = torch.FloatTensor(self.FeatureFrames_List)
        return self.__FeatureFrames_tensor

    @property
    def FeatureFrames_wet_tensor(self):
        if self.__FeatureFrames_wet_tensor is None:
            self.__FeatureFrames_wet_tensor = torch.FloatTensor(self.FeatureFrames_wet_List)
        return self.__FeatureFrames_wet_tensor

    @property
    def FeatureFrames_dry_tensor(self):
        if self.__FeatureFrames_dry_tensor is None:
            self.__FeatureFrames_dry_tensor = torch.FloatTensor(self.FeatureFrames_dry_List)
        return self.__FeatureFrames_dry_tensor

    @property
    def FeatureFrames_unknown_tensor(self):
        if self.__FeatureFrames_unknown_tensor is None:
            self.__FeatureFrames_unknown_tensor = torch.FloatTensor(self.FeatureFrames_unknown_List)
        return self.__FeatureFrames_unknown_tensor

    def __len__(self):
        if (self.FeatureFrames_tensor is None):
            return 0
        return len(self.FeatureFrames_tensor)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = self.FeatureFrames_tensor[idx]
        # numOfRows = int(self.FeatureFrames_tensor[idx].shape[0]/12) # mfcc frame feature length = 12
        # sample = torch.reshape(self.FeatureFrames_tensor[idx], (1, 1, numOfRows, 12))
        return sample

    def set_forceReCulc(self, bool):
        self.forceReCulc = bool

    def get_allAsrFiles_paths(self, allPatientsPaths):
        try:
            allAsrFiles_paths = np.load(str(self.saveLocationUrl) + '\\' + self.saveNameStart + '_allAsrFiles_paths.npz')
            allAsrFiles_paths = allAsrFiles_paths['arr_0']
        except FileNotFoundError:
            allAsrFiles_paths = list()
            # [allAsrFiles_paths.extend(glob.glob(patientPath + "//**//*asr.json", recursive=True) for patientPath in allPatientsPaths)]
            progressbar = tqdm(total=len(allPatientsPaths), position=0, leave=False, disable=self.turnOffProgBars)
            for patientPath, i in zip(allPatientsPaths, range(len(allPatientsPaths))):
                allAsrFiles_paths.extend(glob.glob(patientPath + "//*asr.json", recursive=False))
                # progressbar:
                progressbar.set_description('calculating allAsrFiles_paths...'.format(i))
                progressbar.update(1)
            progressbar.close
            # save list:
            np.savez(str(self.saveLocationUrl) + '\\' + self.saveName, allAsrFiles_paths)

        return allAsrFiles_paths

    def get_allPatients_paths(self):
        d = str(self.dbPath)
        hospitals = list(filter(lambda x: os.path.isdir(os.path.join(d, x)), os.listdir(d)))
        hospitals = [folder for folder in hospitals if len(folder) == 3 and folder.isupper()]
        allPatientsPaths = list()
        for hospital in hospitals:
            path = str(self.dbPath) + '\\' + hospital
            HospitalPatientsDirs = os.listdir(path)
            HospitalPatientsPaths = [path + '\\' + HospitalPatientsDir for HospitalPatientsDir in HospitalPatientsDirs]
            allPatientsPaths.extend(HospitalPatientsPaths)
        return allPatientsPaths

    def get_all_speech_event_features_for_patient(self, patient_folder_path_db, speech_event):
        """"""
        # ------ debug:
        # patient_folder_path_db = r"\\192.168.55.210\f$\db\BLN\BLN-0001"
        # ------
        # patient = Path(patient_folder_path_db).name
        # Create DataFrame to save data
        mfcc_framesBySe_df = pd.DataFrame(columns=["file", "mfcc"])
        # Read all files from directory
        if not self.disablePrints: print("\nRead all files from directory...")
        RelevantRecordingsList, sentenceIdList, sentenceRecordingNumDict = \
            ProcessDirectory(workingDirectory=patient_folder_path_db, patientID=self.patient, inputSentenceIdList="")
        if not self.disablePrints: print("\nFinished reading files from directory")
        clinicalInfo = CordioClinicalInformation(patient=self.patient)
        # progressbar:
        progressbar = tqdm(total=len(RelevantRecordingsList), position=0, leave=False, disable=self.turnOffProgBars)
        for r, i in zip(RelevantRecordingsList, range(len(RelevantRecordingsList))):
            # Extract recording's features
            recording_features = RecordingFeatures(r, clinicalInfo)
            recording_features.extractRecordingFeatures()
            if not recording_features.isValidSentence or not recording_features.isValidRegister: continue
            # Get features from features class
            mfcc = recording_features.MATfeatures["mfcc"]
            #get file name:
            curr_file = r.wavePath.name
            # Get asr labels information for all speech events (se)
            for cur_se in recording_features.allLabels:
                # Extract information only for phoneme speech event
                if cur_se.isValid != True or speech_event != cur_se.label_expected: continue
                # if (type(cur_se) is not PhonemeLabelASR): continue
                se_mfcc = mfcc[cur_se.start_frame_idx:cur_se.end_frame_idx]
                cur_se_feature_dict = {"file": curr_file,
                                        "mfcc": se_mfcc}
                mfcc_framesBySe_df = mfcc_framesBySe_df.append(cur_se_feature_dict, ignore_index=True)
                break
                # progressbar:
            progressbar.set_description('working on:' + curr_file + "...".format(i))
            progressbar.update(1)
        progressbar.close

        return mfcc_framesBySe_df

    def __get_all_speech_event_features_for_patient_parallel_inner__(self, r, clinicalInfo, speech_event, mfcc_framesBySe_df, se_triphon):
        # Extract recording's features
        recording_features = RecordingFeatures(r, clinicalInfo)
        recording_features.extractRecordingFeatures()
        if not recording_features.isValidSentence or not recording_features.isValidRegister: return mfcc_framesBySe_df
        # Get features from features class
        mfcc = recording_features.MATfeatures["mfcc"]
        # get file name:
        curr_file = r.wavePath.name
        # Get asr labels information for all speech events (se)
        for cur_se in recording_features.allLabels:
            # Extract information only for phoneme speech event
            if cur_se.isValid != True or speech_event != cur_se.label_expected: continue
            if (se_triphon is not None) and cur_se.isValid and (se_triphon != cur_se.label_triphone_expected): continue
            # if (type(cur_se) is not PhonemeLabelASR): continue
            se_mfcc = mfcc[cur_se.start_frame_idx:cur_se.end_frame_idx]
            cur_se_feature_dict = {"file": curr_file,
                                   "mfcc": se_mfcc}
            mfcc_framesBySe_df = mfcc_framesBySe_df.append(cur_se_feature_dict, ignore_index=True)
            break
        return mfcc_framesBySe_df

    def get_all_speech_event_features_for_patient_parallel(self, patient_folder_path_db, speech_event, se_triphon):
        """"""
        # ------ debug:
        # patient_folder_path_db = r"\\192.168.55.210\f$\db\BLN\BLN-0001"
        # ------
        # patient = Path(patient_folder_path_db).name
        # Create DataFrame to save data
        mfcc_framesBySe_df = pd.DataFrame(columns=["file", "mfcc"])
        # Read all files from directory
        if not self.disablePrints: print("\nRead all files from directory...")
        # RelevantRecordingsList, sentenceIdList, sentenceRecordingNumDict = \
        #     ProcessDirectory(workingDirectory=patient_folder_path_db, patientID=self.patient, inputSentenceIdList="") #TODO: use ProcessDirectorySentence insted
        language = "Hebrew"
        sentencePhonemeByLanguagePath = r"\\192.168.55.210\f$\Or-rnd\linguisticData\sentencePhonemeByLanguage.xlsx"
        OT = MyTools()
        sentence2phonemByLang, phonem2sentenceByLang = OT.getPhonemeNSentence_dicts(language, sentencePhonemeByLanguagePath)
        sentences = phonem2sentenceByLang[speech_event]
        # RelevantRecordingsList, sentenceIdList, sentenceRecordingNumDict = ProcessDirectorySentence(workingDirectory=patient_folder_path_db, patientID=self.patient, sentenceId=sentence)
        RelevantRecordingsLists = Parallel(n_jobs=self.jobsNum)(delayed(self.My_ProcessDirectorySentence)(patient_folder_path_db, self.patient, sentences[i]) for i in tqdm(range(len(sentences)), desc="get all relevant recordings for "+str(self.patient), disable=self.turnOffProgBars))
        RelevantRecordingsList = []
        [RelevantRecordingsList.extend(RelevantRecordingsList_i) for RelevantRecordingsList_i in RelevantRecordingsLists if RelevantRecordingsList_i != []]
        if not self.disablePrints: print("\nFinished reading files from directory")
        clinicalInfo = CordioClinicalInformation(patient=self.patient)

        if not self.disablePrints: print("\n coping files parallel using " + str(self.jobsNum) + " jobs")
        mfcc_framesBySe_df = Parallel(n_jobs=self.jobsNum)\
            (delayed(self.__get_all_speech_event_features_for_patient_parallel_inner__)
             (RelevantRecordingsList[i], clinicalInfo, speech_event, mfcc_framesBySe_df, se_triphon)
             for i in tqdm(range(len(RelevantRecordingsList)), desc="get all speech event features for "+str(self.patient), disable=self.turnOffProgBars))
        mfcc_framesBySe_df2 = pd.DataFrame(columns=list(mfcc_framesBySe_df[0].columns))
        for framesBySe in mfcc_framesBySe_df:
            if framesBySe.shape[0] > 0:
                mfcc_framesBySe_df2 = mfcc_framesBySe_df2.append(framesBySe)
        mfcc_framesBySe_df2 = mfcc_framesBySe_df2.reset_index(drop=True)

        return mfcc_framesBySe_df2

    def My_ProcessDirectorySentence(self, workingDirectory, patientID, sentencesId):
        RelevantRecordingsList, sentenceRecordingNum = ProcessDirectorySentence(
            workingDirectory=workingDirectory, patientID=patientID, sentenceId=sentencesId)
        return RelevantRecordingsList

    def getInpunLength(self, feature_framesBySe_df, feature):
        """
        function that calculate the length L such that 0.8=P(phoneme length < L)
        feature_framesBySe_df - dataFrame with feature column
        feature - string indicate dataFrame feature ("mfcc")
        """
        PhonemeLengths = [len(feature_framesBySe_df[feature][i]) for i in range(len(feature_framesBySe_df))]
        length_CDF = sns.distplot(PhonemeLengths).get_lines()[0].get_data()
        featureLengthIdx = np.where(length_CDF[1] < 0.8)[0][-1]
        featureLength = length_CDF[1][featureLengthIdx]
        # calculate cdf:
        PhonemeLengths_unique = list(set(PhonemeLengths))
        if 0 in PhonemeLengths_unique: PhonemeLengths_unique.remove(0)
        # featureLength_PDF = np.histogram(PhonemeLengths, bins=PhonemeLengths_unique, density=True)
        featureLength_PDF_tmp = np.unique(PhonemeLengths, return_counts=True)
        featureLength_PDF = []
        featureLength_PDF.append(featureLength_PDF_tmp[0])
        featureLength_PDF.append(featureLength_PDF_tmp[1] / max(featureLength_PDF_tmp[1]))
        featureLength_CDF = [sp.integrate.simps(featureLength_PDF[1][0:i]) for i in range(1, len(featureLength_PDF[0]))]
        featureLength_CDF = featureLength_CDF / max(featureLength_CDF)
        # get the length L such that 0.8=P(phoneme length < L):
        # choosedPhnLen = featureLength_PDF_tmp[0][len(featureLength_CDF[featureLength_CDF<=0.8])]
        chosePhnLen = featureLength_PDF_tmp[0][np.where(featureLength_CDF < 0.8)[0][-1] + 1]

        return chosePhnLen

    def UniformLengthOfPhonemeByGivenLength(self, feature_framesBySe_df, feature, chosePhnLen):
        """Uniform the length of phoneme to chosePhnLen"""
        UniformPhonemeLengths = []
        featurVecLen = len(feature_framesBySe_df[feature][0][0])
        for i in range(len(feature_framesBySe_df)):
            if feature_framesBySe_df[feature][i] == []:
                UniformPhonemeLengths.append([])
                continue
            if len(feature_framesBySe_df[feature][i]) >= chosePhnLen:
                UniformPhonemeLengths.append(feature_framesBySe_df[feature][i][0:chosePhnLen])
            else:
                framesToAdd = chosePhnLen - len(feature_framesBySe_df[feature][i])
                # UniformPhonemeLengths.append(feature_framesBySe_df[feature][i] + [[0]*featurVecLen] * framesToAdd)
                UniformPhonemeLengths.append(list(feature_framesBySe_df[feature][i]) + [[0]*featurVecLen] * framesToAdd)
                # print("i="+str(i)+", append_len="+str(len(UniformPhonemeLengths))) # for debug
        # add to dataframe:
        feature_framesBySe_df["uniform_mfcc"] = UniformPhonemeLengths

        return feature_framesBySe_df

    def addClinicalInfoFeatureFramesDf(self, feature_framesBySe_df):
        clinicalInfo = CordioClinicalInformation(patient=self.patient)
        clinicalInfoList = []
        for file in feature_framesBySe_df["file"]:
            f = CordioFile(file)
            clinicalInfoList.append(self.ClinicalInfoDict[clinicalInfo(f.recordingDatetime)])
        feature_framesBySe_df["clinicalInfo"] = clinicalInfoList

        return feature_framesBySe_df

    def getNoneUniformLenFeatureFrames_df(self, forceReCulc, feature):
        # checking for existing file before calculation:
        savePath = str(self.saveLocationUrl / (self.saveNameStart + "NoneUniformLenFeatureFrames_df.json"))
        if not forceReCulc and os.path.isfile(savePath):
            # print("loading extracted feature from: " + savePath + "...")
            NoneUniformLenFeature_framesBySe_df = pd.read_json(savePath)
            # print("extracted feature loaded." + savePath + "...")
        else:
            if not self.disablePrints: print("extracting features (this will take some time)...")
            NoneUniformLenFeature_framesBySe_df = self.get_all_speech_event_features_for_patient_parallel(self.patient_folder_path_db,
                                                                                            speech_event=self.speechEvent, se_triphon=self.se_triphon)
            if not self.disablePrints: print("saving extracted feature as: " + savePath + "...")
            NoneUniformLenFeature_framesBySe_df.to_json(savePath)
            if not self.disablePrints: print("extracted features file saved as: " + savePath + ".")
        return NoneUniformLenFeature_framesBySe_df

    def getFeatureFrames_df(self, forceReCulc, feature):
        # choosing system input length:
        # chosePhnLen = self.getInpunLength(feature_framesBySe_df, feature)
        # uniform output:
        feature_framesBySe_df = self.UniformLengthOfPhonemeByGivenLength(self.NoneUniformLenFeature_framesBySe_df, feature, self.chosenPhnLen)
        # cutting or zero-padding:
        feature_framesBySe_df = self.addClinicalInfoFeatureFramesDf(feature_framesBySe_df)
        # self.FeatureFrames_df = feature_framesBySe_df
        # self.FeatureFrames_List = feature_framesBySe_df["uniform_mfcc"]
        # self.FeatureFrames_tensor = torch.FloatTensor(feature_framesBySe_df["uniform_mfcc"])
        # return self.FeatureFrames_df, self.FeatureFrames_List, self.FeatureFrames_tensor
        return feature_framesBySe_df

    def __FeatureFrames_dfToList__(self, feature_framesBySe_df, clinicaLabel="None"):
        tmp_FeatureFrames_List = []
        tmp_FeatureFramesDates_List = []
        mt = MyTools()

        for patientFeatureFrames, patientFeatureLabel, fileName in \
                zip(self.FeatureFrames_df["uniform_mfcc"],self.FeatureFrames_df["clinicalInfo"], self.FeatureFrames_df["file"]):
            listToAppend = list(mt.flatten(patientFeatureFrames))
            if listToAppend != [] and clinicaLabel == None:
                tmp_FeatureFrames_List.append(listToAppend)
                tmp_FeatureFramesDates_List.append(CordioFile(fileName).recordingDatetime)
            elif listToAppend != [] and patientFeatureLabel == clinicaLabel:
                tmp_FeatureFrames_List.append(listToAppend)  # remove entry's with 0 frames:
                tmp_FeatureFramesDates_List.append(CordioFile(fileName).recordingDatetime)

        return tmp_FeatureFrames_List, tmp_FeatureFramesDates_List

class preprocessData_multiTriphon:
    def __init__(self, patient, speechEvent_id, dbPath=r"\\192.168.55.210\db",
                 saveLocationUrl=r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump", saveName="NoName", jobsNum=1,
                 forceReCulc=False, feature="mfcc", se_triphon=None, disablePrints=False, turnOffProgBars=False):

        self.NO_CLINICAL_STATUS = -1.0
        self.CLINICAL_STATUS_DRY = 0.0
        self.CLINICAL_STATUS_UNKNOWN = 0.5
        self.CLINICAL_STATUS_WET = 1.0
        self.ClinicalInfoDict = {self.CLINICAL_STATUS_DRY: "dry",
                                 self.CLINICAL_STATUS_UNKNOWN: "unknown",
                                 self.CLINICAL_STATUS_WET: "wet",
                                 self.NO_CLINICAL_STATUS: "patient was not in the experiment in this date"}

        self.FEATURE_LEN_CODEX = {'mfcc': 12}
        self.patient = patient
        self.hospital = patient[0:3]
        self.feature = feature
        # self.saveName = saveName
        self.patient_folder_path_db = os.path.join(dbPath, self.hospital+"\\"+self.patient)
        self.speechEvent_id = speechEvent_id
        self.dbPath = Path(dbPath)
        self.saveLocationUrl = Path(saveLocationUrl)
        self.jobsNum = jobsNum
        self.forceReCulc = forceReCulc
        if (se_triphon[0]=="/" and  se_triphon[-1]=="/"): self.se_triphon = se_triphon[1:-1]
        else: self.se_triphon = se_triphon
        self.disablePrints = disablePrints
        self.turnOffProgBars = turnOffProgBars
        if not isinstance(self.speechEvent_id, list): self.speechEvent_id = [self.speechEvent_id]
        if not isinstance(self.se_triphon, list): self.se_triphon = [self.se_triphon]
        if len(self.se_triphon) != len(self.speechEvent_id): raise ValueError("se_triphon length is different from speechEvent length")

        if len(self.speechEvent_id) > 7: str_SEid = str(self.speechEvent_id[0:7])[0:-1] + ", more_]"
        else: str_SEid = str(self.speechEvent_id)
        self.saveNameStart = self.patient + '_' + self.feature + '_' + str_SEid


        self.FeatureFramesDates_wet_List = None
        self.FeatureFramesDates_dry_List = None
        self.FeatureFramesDates_unknown_List = None
        self.__NoneUniformLenFeature_framesBySe_df = None
        self.__FeatureFrames_wet_tensor = None
        self.__FeatureFrames_dry_tensor = None
        self.__FeatureFrames_unknown_tensor = None
        self.__chosenPhnLen = None
        self.__FeatureFrames_df = None
        self.__FeatureFrames_List = None
        self.__FeatureFrames_wet_List = None
        self.__FeatureFrames_dry_List = None
        self.__FeatureFrames_unknown_List = None
        self.__FeatureFrames_tensor = None

        # self.FeatureFrames_df = self.preprocessDataMultiSE(jobsNum=self.jobsNum, forceReCulc=False,
        #                                                    turnOffProgBar=False)

    @property
    def NoneUniformLenFeature_framesBySe_df(self):
        if self.__NoneUniformLenFeature_framesBySe_df is None:
            self.__NoneUniformLenFeature_framesBySe_df = self.getNoneUniformLenFeatureFrames_df(self.forceReCulc)
        return self.__NoneUniformLenFeature_framesBySe_df
    @property
    def FeatureFrames_df(self):
        if self.__FeatureFrames_df is None:
            self.__FeatureFrames_df = self.getFeatureFrames_df(self.feature)
        return self.__FeatureFrames_df

    @property
    def chosenPhnLen(self):
        if (self.__chosenPhnLen is not None) and np.isnan(self.__chosenPhnLen):
            warnings.warn("chosenPhnLen isn't available, reason: maybe triphone/phoneme isn't available")
        if self.__chosenPhnLen is None:
            self.__chosenPhnLen = self.getInpunLength(self.NoneUniformLenFeature_framesBySe_df, self.feature)
        return self.__chosenPhnLen

    @chosenPhnLen.setter
    def chosenPhnLen(self, value):
        self.__chosenPhnLen = value

    @property
    def FeatureFrames_dry_tensor(self):
        if self.__FeatureFrames_dry_tensor is None:
            self.__FeatureFrames_dry_tensor = torch.FloatTensor(self.FeatureFrames_dry_List)
        return self.__FeatureFrames_dry_tensor
    @property
    def FeatureFrames_dry_List(self):
        if self.__FeatureFrames_dry_List is None:
            self.__FeatureFrames_dry_List, self.FeatureFramesDates_dry_List = \
                self.__FeatureFrames_dfToList__(clinicaLabel="dry")
        return self.__FeatureFrames_dry_List

    @property
    def FeatureFrames_wet_tensor(self):
        if self.__FeatureFrames_wet_tensor is None:
            self.__FeatureFrames_wet_tensor = torch.FloatTensor(self.FeatureFrames_wet_List)
        return self.__FeatureFrames_wet_tensor
    @property
    def FeatureFrames_wet_List(self):
        if self.__FeatureFrames_wet_List is None:
            self.__FeatureFrames_wet_List, self.FeatureFramesDates_wet_List = \
                self.__FeatureFrames_dfToList__(clinicaLabel="wet")
        return self.__FeatureFrames_wet_List

    @property
    def FeatureFrames_unknown_tensor(self):
        if self.__FeatureFrames_unknown_tensor is None:
            self.__FeatureFrames_unknown_tensor = torch.FloatTensor(self.FeatureFrames_unknown_List)
        return self.__FeatureFrames_unknown_tensor
    @property
    def FeatureFrames_unknown_List(self):
        if self.__FeatureFrames_unknown_List is None:
            self.__FeatureFrames_unknown_List, self.FeatureFramesDates_unknown_List = \
                self.__FeatureFrames_dfToList__(clinicaLabel="unknown")
        return self.__FeatureFrames_unknown_List

    # ------------------------------------------------------------------------------------------------------------------
    # ----------------------------------------------------Methods--------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------

    def __FeatureFrames_dfToList__(self, clinicaLabel="None"):

        if self.FeatureFrames_df.empty: return [], []

        tmp_FeatureFrames_List = []
        tmp_FeatureFramesDates_List = []
        mt = MyTools()

        for patientFeatureFrames, patientFeatureLabel, fileName in zip(self.FeatureFrames_df["uniform_mfcc"], self.FeatureFrames_df["clinicalInfo"], self.FeatureFrames_df["file"]):
            listToAppend = list(mt.flatten(patientFeatureFrames))
            if listToAppend != [] and clinicaLabel == None:
                tmp_FeatureFrames_List.append(listToAppend)
                tmp_FeatureFramesDates_List.append(CordioFile(fileName).recordingDatetime)
            elif listToAppend != [] and patientFeatureLabel == clinicaLabel:
                tmp_FeatureFrames_List.append(listToAppend)  # remove entry's with 0 frames:
                tmp_FeatureFramesDates_List.append(CordioFile(fileName).recordingDatetime)

        return tmp_FeatureFrames_List, tmp_FeatureFramesDates_List

    def __singlePreprocessedData__(self, turnOffProgBars=False, disablePrints=False, forceReCulc=False):

        fullData = preprocessData_singleTriphon(patient=self.patient, speechEvent=self.speechEvent_id, dbPath=self.dbPath,
                                                saveLocationUrl=self.saveLocationUrl, saveName=self.saveName,
                                                jobsNum=self.jobsNum, forceReCulc=forceReCulc,
                                                feature=self.feature, se_triphon=self.se_triphon,
                                                disablePrints=disablePrints, turnOffProgBars=turnOffProgBars)

        df = fullData.NoneUniformLenFeature_framesBySe_df
        # adding triphon label
        df["triphone"] = [self.se_triphon]*len(df)
        # adding datetime
        datesList = [CordioFile(file).recordingDatetime for file in df["file"]]
        df["datetime"] = datesList

        return df.to_dict()

    def preprocessDataMultiSE(self, jobsNum=1, forceReCulc=False, turnOffProgBar=False):
        if jobsNum > 1: innerJobsNum = 1
        turnOffProgBars = True
        # if not isinstance(self.speechEvent, list): self.speechEvent = [self.speechEvent]
        # if not isinstance(self.se_triphon, list): self.se_triphon = [self.se_triphon]
        # if len(self.se_triphon) != len(self.speechEvent): raise ValueError("se_triphon length is different from speechEvent length")

        # check for saved data:
        fullSavedFilrPath = Path(self.saveLocationUrl) / Path(str(self.se_triphon)+"_featureData_dict.pickle")
        if Path.is_file(fullSavedFilrPath):
            # load dict:
            with open(str(fullSavedFilrPath), 'rb') as handle:
                dictList = pickle.load(handle)
        else:
            dictList = Parallel(n_jobs=self.jobsNum)(delayed(self.__singlePreprocessedData__)
                                                     (self.patient, self.speechEvent_id[i], self.dbPath,
                                                      self.saveLocationUrl, self.saveName, innerJobsNum, forceReCulc,
                                                      self.feature, self.se_triphon[i], turnOffProgBars)
                                                     for i in tqdm(range(len(self.se_triphon)),
                                                                   desc="preprocessing data to "+str(self.speechEvent_id),
                                                                   disable=turnOffProgBar))
            # save dict:
            with open(str(fullSavedFilrPath), 'wb') as handle:
                pickle.dump(dictList, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # from dictionary to df:
        df_list = []
        for dict in dictList:
            df_list.append(pd.DataFrame.from_dict(dict))
        df = pd.concat(df_list)
        return df

    def getFeatureFrames_df(self, feature):
        # uniform output:
        feature_framesBySe_df = self.UniformLengthOfPhonemeByGivenLength(self.NoneUniformLenFeature_framesBySe_df, feature)
        if feature_framesBySe_df.empty: return feature_framesBySe_df

        # cutting or zero-padding:
        feature_framesBySe_df = self.addClinicalInfoFeatureFramesDf(feature_framesBySe_df)
        return feature_framesBySe_df

    def UniformLengthOfPhonemeByGivenLength(self, feature_framesBySe_df, feature):
        """Uniform the length of phoneme to chosePhnLen"""
        if np.isnan(self.chosenPhnLen):
            warnings.warn("cant create Uniform Length Phoneme data. chosenPhnLen isn't available, reason: maybe triphone/phoneme isn't available")
            return np.nan
        UniformPhonemeLengths = []
        # featurVecLen = len(feature_framesBySe_df[feature][0][0])
        featurVecLen = self.FEATURE_LEN_CODEX[feature]
        for i in range(len(feature_framesBySe_df)):
            if feature_framesBySe_df[feature][i] == []:
                UniformPhonemeLengths.append([])
                continue
            if len(feature_framesBySe_df[feature][i]) >= self.chosenPhnLen:
                UniformPhonemeLengths.append(feature_framesBySe_df[feature][i][0:self.chosenPhnLen])
            else:
                framesToAdd = self.chosenPhnLen - len(feature_framesBySe_df[feature][i])
                UniformPhonemeLengths.append(list(feature_framesBySe_df[feature][i]) + [[0]*featurVecLen] * framesToAdd)
        # add to dataframe:
        feature_framesBySe_df["uniform_mfcc"] = UniformPhonemeLengths

        return feature_framesBySe_df

    def getInpunLength(self, NoneUniformLenFeature_framesBySe_df, feature):
        """
        function that calculate the length L such that 0.8=P(phoneme length < L)
        NoneUniformLenFeature_framesBySe_df - dataFrame with feature column
        feature - string indicate dataFrame feature ("mfcc")
        """
        PhonemeLengths = [len(NoneUniformLenFeature_framesBySe_df[feature][i]) for i in range(len(NoneUniformLenFeature_framesBySe_df))]
        if len(PhonemeLengths) == 0:
            warnings.warn("chosenPhnLen isn't available, reason: maybe triphone/phoneme isn't available")
            return np.nan
        length_CDF = sns.distplot(PhonemeLengths).get_lines()[0].get_data()
        featureLengthIdx = np.where(length_CDF[1] < 0.8)[0][-1]
        featureLength = length_CDF[1][featureLengthIdx]
        # calculate cdf:
        PhonemeLengths_unique = list(set(PhonemeLengths))
        if 0 in PhonemeLengths_unique: PhonemeLengths_unique.remove(0)
        # featureLength_PDF = np.histogram(PhonemeLengths, bins=PhonemeLengths_unique, density=True)
        featureLength_PDF_tmp = np.unique(PhonemeLengths, return_counts=True)
        featureLength_PDF = []
        featureLength_PDF.append(featureLength_PDF_tmp[0])
        featureLength_PDF.append(featureLength_PDF_tmp[1] / max(featureLength_PDF_tmp[1]))
        featureLength_CDF = [sp.integrate.simps(featureLength_PDF[1][0:i]) for i in range(1, len(featureLength_PDF[0]))]
        featureLength_CDF = featureLength_CDF / max(featureLength_CDF)
        # get the length L such that 0.8=P(phoneme length < L):
        # choosedPhnLen = featureLength_PDF_tmp[0][len(featureLength_CDF[featureLength_CDF<=0.8])]
        chosePhnLen = featureLength_PDF_tmp[0][np.where(featureLength_CDF < 0.8)[0][-1] + 1]

        return chosePhnLen

    def getNoneUniformLenFeatureFrames_df(self, forceReCulc=False):
        # checking for existing file before calculation:
        if len(self.speechEvent_id) > 7: str_SEid = str(self.speechEvent_id[0:7])[0:-1] + ", more_]"
        else: str_SEid = str(self.speechEvent_id)
        saveName = self.patient + '_' + self.feature + '_' + str_SEid + '_NoneUniformLenFeatureFrames_df'
        # savePath = str(self.saveLocationUrl / (saveName + ".json"))
        savePath = str(self.saveLocationUrl / (saveName + ".json"))
        if not forceReCulc and os.path.isfile(savePath):
            # print("loading extracted feature from: " + savePath + "...")
            NoneUniformLenFeature_framesBySe_df = pd.read_json(savePath, orient='table')
            # check data:
            for i in range(len(NoneUniformLenFeature_framesBySe_df['mfcc'])):
                for j in range(len(NoneUniformLenFeature_framesBySe_df['mfcc'][i])):
                    if any(np.isnan(NoneUniformLenFeature_framesBySe_df['mfcc'][i][j])):
                        raise ValueError("NaN in NoneUniformLenFeature_framesBySe_df: "+str(i)+", "+str(j))

            # NoneUniformLenFeature_framesBySe_df = pd.read_feather(savePath)
            # print("extracted feature loaded." + savePath + "...")
        else:
            if not self.disablePrints: print("extracting features (this will take some time)...")
            NoneUniformLenFeature_framesBySe_df = self.get_all_speech_event_features_for_patient_parallel(self.patient_folder_path_db,
                                                                                                          speechEvent_id=self.speechEvent_id, se_triphon=self.se_triphon)
            # check data:
            for i in range(len(NoneUniformLenFeature_framesBySe_df['mfcc'])):
                for j in range(len(NoneUniformLenFeature_framesBySe_df['mfcc'][i])):
                    if any(np.isnan(NoneUniformLenFeature_framesBySe_df['mfcc'][i][j])):
                        raise ValueError("NaN in NoneUniformLenFeature_framesBySe_df: " + str(i) + ", " + str(j))

            if not self.disablePrints: print("saving extracted feature as: " + savePath + "...")
            NoneUniformLenFeature_framesBySe_df.to_json(savePath, orient='table')
            # NoneUniformLenFeature_framesBySe_df.to_feather(savePath)
            if not self.disablePrints: print("extracted features file saved as: " + savePath + ".")
        return NoneUniformLenFeature_framesBySe_df

    def get_all_speech_event_features_for_patient_parallel(self, patient_folder_path_db, speechEvent_id, se_triphon):
        """"""
        # ------ debug:
        # patient_folder_path_db = r"\\192.168.55.210\f$\db\BLN\BLN-0001"
        # ------
        # patient = Path(patient_folder_path_db).name
        # Create DataFrame to save data
        mfcc_framesBySe_df = pd.DataFrame(columns=["file", "mfcc"])
        # Read all files from directory
        if not self.disablePrints: print("\nRead all files from directory...")
        # RelevantRecordingsList, sentenceIdList, sentenceRecordingNumDict = \
        #     ProcessDirectory(workingDirectory=patient_folder_path_db, patientID=self.patient, inputSentenceIdList="") #TODO: use ProcessDirectorySentence insted
        language = "Hebrew"
        sentencePhonemeByLanguagePath = r"\\192.168.55.210\f$\Or-rnd\linguisticData\sentencePhonemeByLanguage.xlsx"
        OT = MyTools()
        # sentence2phonemByLang, phonem2sentenceByLang = OT.getPhonemeNSentence_dicts(language, sentencePhonemeByLanguagePath)
        speechEvent_id_arrays = []
        for se_id in speechEvent_id:
            speechEvent_id_array_element = [int(d) for d in str(se_id)]
            speechEvent_id_arrays.append(speechEvent_id_array_element)
        sentences = [''.join(map(str, se_id[1:5])) for se_id in speechEvent_id_arrays]
        sentences = list(set(sentences))
        # RelevantRecordingsList, sentenceIdList, sentenceRecordingNumDict = ProcessDirectorySentence(workingDirectory=patient_folder_path_db, patientID=self.patient, sentenceId=sentence)
        RelevantRecordingsLists = Parallel(n_jobs=self.jobsNum)(delayed(self.My_ProcessDirectorySentence)(patient_folder_path_db, self.patient, sentences[i])
                                                                for i in tqdm(range(len(sentences)), desc="get all relevant recordings for "+str(self.patient), disable=self.turnOffProgBars))
        RelevantRecordingsList = []
        [RelevantRecordingsList.extend(RelevantRecordingsList_i) for RelevantRecordingsList_i in RelevantRecordingsLists if RelevantRecordingsList_i != []]
        if RelevantRecordingsList == []: return pd.DataFrame()
        if not self.disablePrints: print("\nFinished reading files from directory")
        clinicalInfo = CordioClinicalInformation(patient=self.patient)

        if not self.disablePrints: print("\n coping files parallel using " + str(self.jobsNum) + " jobs")
        mfcc_framesBySe_df = Parallel(n_jobs=self.jobsNum)\
            (delayed(self.__get_all_speech_event_features_for_patient_parallel_inner__)
             (RelevantRecordingsList[i], clinicalInfo, speechEvent_id, mfcc_framesBySe_df, se_triphon)
             for i in tqdm(range(len(RelevantRecordingsList)), desc="get all speech event features for "+str(self.patient), disable=self.turnOffProgBars))
        mfcc_framesBySe_df2 = pd.DataFrame(columns=list(mfcc_framesBySe_df[0].columns))
        for framesBySe in mfcc_framesBySe_df:
            if framesBySe.shape[0] > 0:
                mfcc_framesBySe_df2 = mfcc_framesBySe_df2.append(framesBySe)
        mfcc_framesBySe_df2 = mfcc_framesBySe_df2.reset_index(drop=True)
        # check data:
        for i in range(len(mfcc_framesBySe_df2['mfcc'])):
            for j in range(len(mfcc_framesBySe_df2['mfcc'][i])):
                if any(np.isnan(mfcc_framesBySe_df2['mfcc'][i][j])):
                    raise ValueError("NaN in mfcc_framesBySe_df2: " + str(i) + ", " + str(j))

        return mfcc_framesBySe_df2

    def My_ProcessDirectorySentence(self, workingDirectory, patientID, sentencesId):
        RelevantRecordingsList, sentenceRecordingNum = ProcessDirectorySentence(
            workingDirectory=workingDirectory, patientID=patientID, sentenceId=sentencesId)
        return RelevantRecordingsList

    def __get_all_speech_event_features_for_patient_parallel_inner__(self, r, clinicalInfo, speechEvent_id, mfcc_framesBySe_df, se_triphon):
        # Extract recording's features
        recording_features = RecordingFeatures(r, clinicalInfo)
        recording_features.extractRecordingFeatures()
        if not recording_features.isValidSentence or not recording_features.isValidRegister: return mfcc_framesBySe_df
        # Get features from features class
        mfcc = recording_features.MATfeatures["mfcc"]
        # get file name:
        curr_file = r.wavePath.name
        # Get asr labels information for all speech events (se)
        for cur_se in recording_features.allLabels:
            # Extract information only for phoneme speech event
            # if cur_se.isValid != True or speech_event != cur_se.label_expected: continue
            if (cur_se.isValid != True) or (cur_se.asr_expected_id not in speechEvent_id): continue
            # if (se_triphon is not None) and cur_se.isValid and (se_triphon != cur_se.label_triphone_expected): continue
            # if (se_triphon is None) or (not cur_se.isValid) or (cur_se.label_triphone_expected not in se_triphon): continue
            # if (type(cur_se) is not PhonemeLabelASR): continue
            se_mfcc = mfcc[cur_se.start_frame_idx:cur_se.end_frame_idx]
            if np.isnan(se_mfcc).any():
                warnings.warn("NaN in se_mfcc (feature extraction)")
                continue
            cur_se_feature_dict = {"file": curr_file,
                                   "phn_id": str(cur_se.asr_expected_id),
                                   "phn_label": cur_se.label_expected,
                                   "mfcc": se_mfcc}
            mfcc_framesBySe_df = mfcc_framesBySe_df.append(cur_se_feature_dict, ignore_index=True)
        # print("len(mfcc_framesBySe_df): "+str(len(mfcc_framesBySe_df)))
        # check data:
        for i in range(len(mfcc_framesBySe_df['mfcc'])):
            for j in range(len(mfcc_framesBySe_df['mfcc'][i])):
                if any(np.isnan(mfcc_framesBySe_df['mfcc'][i][j])):
                    raise ValueError("NaN in mfcc_framesBySe_df: " + str(i) + ", " + str(j))
        return mfcc_framesBySe_df

    def addClinicalInfoFeatureFramesDf(self, feature_framesBySe_df):
        if feature_framesBySe_df.empty: return feature_framesBySe_df

        clinicalInfo = CordioClinicalInformation(patient=self.patient)
        clinicalInfoList = []
        for file in feature_framesBySe_df["file"]:
            f = CordioFile(file)
            clinicalInfoList.append(self.ClinicalInfoDict[clinicalInfo(f.recordingDatetime)])
        feature_framesBySe_df["clinicalInfo"] = clinicalInfoList

        return feature_framesBySe_df

# --------------------------------------
# debug Main ---------------------------
# --------------------------------------
# patient_folder_path_db = r"\\192.168.55.210\f$\db\BLN\BLN-0001"
# patient = Path(patient_folder_path_db).name
# speechEvent = 'i'
# sentence = "S0007"
# hospital = "BLN"
# patient = "BLN-0001"
# # dbug_obj = CordioAE_preprocessData(patient, speechEvent, dbPath=r"\\192.168.55.210\db", saveLocationUrl=r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump", saveName="NoName")
#
# saveLocationUrl = r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump"
# saveName = "mfcc_framesBySe_df"
# feature = "mfcc"
# forceReCulc = False
# jobsNum=30
#
# dbug_obj = CordioAE_preprocessData(patient, speechEvent, dbPath=r"\\192.168.55.210\db",
#                  saveLocationUrl=saveLocationUrl, saveName=saveName, jobsNum=jobsNum)
# FeatureFrames_df = dbug_obj.FeatureFrames_df
# FeatureFrames_List = dbug_obj.FeatureFrames_List
# FeatureFrames_tensor = dbug_obj.FeatureFrames_tensor
#
# # FeatureFrames_df, FeatureFrames_List, FeatureFrames_tensor = dbug_obj.getFeatureFrames_df(forceReCulc, feature)
# print("DB Start!")
# print("DB End?")