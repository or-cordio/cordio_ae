"""
this script creat zip containing all .json, .pkl and ScalarAsrDtwDistSummary...csv files by given patients in .xlsx or .csv
zip file will be upload to: s3://cordio-research-test/uploadToServer/

* localZipDestFolder - local path to save zip file
* outputZipFileName = zip file name
* localRootPath = local path to db that contains th pkl and json files for each patient
* localPatientsFile = local path to xlsx or csv file that contains the columns:
    ** patientID - patient name
    ** toRun - 1 for coping patient data and 0 otherwise
* tmpDirPath = path to a tmp directory

   Cordio Medical - Confidential
     Version: 0.3    2020-07-15

     Revision History:
     |   Ver    | Author    | Date           | Change Description
     |----------|-----------|----------------|--------------------
     |   0.1    | Or        | 2020-07-13     | Initial
     |   0.2    | Or        | 2020-07-13     | added sentence filter
     |   0.3    | Or        | 2020-07-15     | added distance table upload option & parallel local copy & fixed tmp dir location
 """

import pandas as pd
import glob, os
from tqdm import tqdm
from pathlib import Path
import tempfile
import shutil

def get_sentence(string):
    string = Path(string)
    if os.path.splitext(str(string))[-1] == ".csv":
        sentence = string.name.split("_")[3]
    if os.path.splitext(str(string))[-1] == ".pickle":
        sentence = string.name.split("_")[3]
    if os.path.splitext(str(string))[-1] == ".json":
        sentence = string.name.split("_")[3]

    return sentence


def get_patient(string):
    string = Path(string)
    if os.path.splitext(str(string))[-1] == ".csv":
        patient = string.name.split("_")[2].upper()
    if os.path.splitext(str(string))[-1] == ".pickle":
        patient = string.name.split("_")[0].upper()
    if os.path.splitext(str(string))[-1] == ".json":
        patient = string.name.split("_")[0].upper()

    return patient


# paths(hard coded data):
localZipDestFolder = r"\\192.168.55.210\f$\Or-rnd\S3_upload\partialHYA"
outputZipFileName = "partialHYA"
localRootPath = r"\\192.168.55.210\f$\db"
# localPatientsFile = r"\\192.168.55.210\f$\Or-rnd\S3_upload\patients_HYA_MS6-create_s3_upload_list_txt_file.xlsx"
localPatientsFile = r"\\192.168.55.210\f$\Or-rnd\S3_upload\MS6-analysisPatients-HYA.csv"
# tmpDirPath = r"\\192.168.55.210\f$\Or-rnd\tmp"
tmpDirPath = r"F:\Or-rnd\tmp"
localZipDestFolder = Path(localZipDestFolder)
localRootPath = Path(localRootPath)
localPatientsFile = Path(localPatientsFile)
tmpDirPath = Path(tmpDirPath)
# what to zip and upload?
is_asr_jsons = False
is_pickles = False
is_distTables = True
# parllel computhing?
parallel = True
jobNum = 20


# get patients to copy:
if os.path.splitext(str(localPatientsFile))[-1] == ".csv":
    patientsTable = pd.read_csv(str(localPatientsFile))
elif os.path.splitext(str(localPatientsFile))[-1] == ".xlsx":
    patientsTable = pd.read_excel(str(localPatientsFile), sheet_name="patients_MS6")
# filter by "toRun":
patientsTable = patientsTable[patientsTable["toRun"]==1]
patientsPaths = []
patients = patientsTable["patientID"]
patients = [patient[0:3]+"-"+patient[3:] for patient in patients if "-" not in patient]
patientsPaths = [localRootPath / Path(str(patient)[0:3]) / patient for patient in patients]

# patientsPaths = [localRootPath+"\\"+patient[0:3]+"\\"+patient for patient in patients]
asrJasonNPklPaths = []
if is_asr_jsons:
    [asrJasonNPklPaths.extend(glob.glob(str(patientPath) + "//*asr.json", recursive=False)) for patientPath in patientsPaths]
if is_pickles:
    [asrJasonNPklPaths.extend(glob.glob(str(patientPath) + "//*.pickle", recursive=False)) for patientPath in patientsPaths]
if is_distTables:
    patientsDistTablsPaths = [patientsPath / "results" / "CordioPythonDistanceCalculation" for patientsPath in patientsPaths]
    [asrJasonNPklPaths.extend(glob.glob(str(patientsDistTablsPath) + "//*.csv", recursive=False)) for patientsDistTablsPath in patientsDistTablsPaths]
# filter each patient by his sentence
progressbar = tqdm(total=len(asrJasonNPklPaths), position=0, leave=False)
patientSentenceDict = dict(zip(patients, patientsTable["sentence"]))
filtered_asrJasonNPklPaths = []
for asrJasonNPklPath_idx in range(len(asrJasonNPklPaths)):
    asrJasonNPklPath = Path(asrJasonNPklPaths[asrJasonNPklPath_idx])
    asrJasonNPklPath_patient = get_patient(asrJasonNPklPath)
    asrJasonNPklPath_sentence = get_sentence(asrJasonNPklPath)
    if patientSentenceDict[asrJasonNPklPath_patient] == asrJasonNPklPath_sentence:
        filtered_asrJasonNPklPaths.append(str(asrJasonNPklPath))
        # asrJasonNPklPaths.remove(str(asrJasonNPklPath))
    # progressbar:
    progressbar.set_description('filtering data by sentence:' + Path(asrJasonNPklPath).name + "...".format(asrJasonNPklPath_idx))
    progressbar.update(1)
progressbar.close
asrJasonNPklPaths = filtered_asrJasonNPklPaths
# local copy and zip:
#   ->local copy at temp directory:
progressbar = tqdm(total=len(asrJasonNPklPaths), position=0, leave=False)
os.mkdir(str(tmpDirPath)) # create tmp directory
# tmpDirPath = tempfile.mkdtemp(str(tempDir))
if parallel == True:
    print("\n coping files parallel using " + str(jobNum) + " jobs")
    from joblib import Parallel, delayed
    Parallel(n_jobs=jobNum)(delayed(shutil.copyfile)(str(asrJasonNPklPaths[i]), str(Path(tmpDirPath) / Path(Path(asrJasonNPklPaths[i]).name)))
                            for i in tqdm(range(len(asrJasonNPklPaths))))

if parallel == False:
    for asrJasonNPklPath, i in zip(asrJasonNPklPaths,range(len(asrJasonNPklPaths))):
        src = str(asrJasonNPklPath)
        dst = str(Path(tmpDirPath) / Path(Path(asrJasonNPklPath).name))

        # progressbar:
        progressbar.set_description('coping:' + Path(asrJasonNPklPath).name + "...".format(i))
        progressbar.update(1)

        shutil.copyfile(src, dst)

    progressbar.close
#   ->Zip local copy at temp directory:
zipLocalDestination = localZipDestFolder / Path(outputZipFileName)
print("\ncreating zip file at: "+str(localZipDestFolder)+"\\"+outputZipFileName+"...")
shutil.make_archive(str(zipLocalDestination), 'zip', tmpDirPath)
print("\nzip file avalble at: "+str(zipLocalDestination))
shutil.rmtree(tmpDirPath) # delete temp directory

# uploading zip files to s3:
print("\nuploading zip to server at: "+" s3://cordio-research-test/uploadToServer/"+str(zipLocalDestination.name)+"...")
bashCommand = "aws s3 cp "+str(zipLocalDestination)+".zip"+" s3://cordio-research-test/"+str(zipLocalDestination.name)+".zip"
os.system(bashCommand)
print("\nuploading zip to server at: "+" s3://cordio-research-test/"+str(zipLocalDestination.name))