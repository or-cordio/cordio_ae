import warnings
from multiprocessing.dummy import freeze_support

import torch
import math
import pandas as pd
import pickle
import json
from collections import OrderedDict
from tqdm import tqdm
from joblib import Parallel, delayed
from datetime import datetime
import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.dates import date2num
from datetime import datetime
# import feather
import platform
from typing import Union
import ast
import heapq

from torch import optim, nn, cat
from pathlib import Path
from CordioAutoEncoder import AE_Net, AE_deepNet, AE_FC_Net, AE_train, AE_test, AE_Net_manual_weight_init
from CordioAE_preprocessData import preprocessData_singleTriphon, preprocessData_multiTriphon
from infrastructure.patientsInformation.CordioPatientClinicalInformation import CordioClinicalInformation
from utils.AnalysisTools.CordioEvalGraphCorrelation import CordioEvalGraphCorrelation
from asr.CordioLexicon import Lexicon
from utils.GeneralTools import Tools

from OrTools import MyTools

# import plotly.express as px

# from Epochsviz.epochsviz import Epochsviz

# warnings.filterwarnings('ignore')
# with warnings.catch_warnings():
warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')
warnings.simplefilter(action='ignore', category=FutureWarning)

# initializing objects:
# --------------------
#  use gpu if available
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")
# create a model from `AE` autoencoder class
# load it to the specified device, either gpu or cpu
model = AE_Net().to(device)
model.apply(AE_Net_manual_weight_init)

# create an optimizer object
# Adam optimizer with learning rate 1e-3
LR = 1e-2
optimizer = optim.Adam(model.parameters(), lr=LR)
# mean-squared error loss
criterion = nn.MSELoss()

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Auxiliary Functions-----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

# preprocessing functions:

def createClinicalInfoDF(clinicalInfoState: str, fullData_multiPhn):
    dataColName = clinicalInfoState + "tensor_data"
    datesColName = clinicalInfoState + "_DatesTime"
    # df = pd.DataFrame(columns=[clinicalInfoState+"tensor_data", clinicalInfoState+"_DatesTime", "triphon", "triphonID"])
    all_data = getattr(fullData_multiPhn, "FeatureFrames_" + clinicalInfoState + "_tensor")
    # all_data = fullData_multiPhn.FeatureFrames_dry_tensor
    all_data_DatesTime = getattr(fullData_multiPhn, "FeatureFramesDates_" + clinicalInfoState + "_List")
    # all_data_DatesTime = fullData_multiPhn.FeatureFramesDates_dry_List
    # create triphone list:
    # if isinstance(fullData_multiPhn.se_triphon, list):
    #     triphonList = fullData_multiPhn.se_triphon * len(all_data_DatesTime)
    # else:
    #     triphonList = [fullData_multiPhn.se_triphon] * len(all_data_DatesTime)
    triphonList = [fullData_multiPhn.se_triphon] * len(all_data_DatesTime)
    # create speechEvent id list:
    # if isinstance(fullData_multiPhn.se_triphon, list):
    #     speechEvent_idList = fullData_multiPhn.speechEvent_id * len(all_data_DatesTime)
    # else:
    #     speechEvent_idList = [fullData_multiPhn.speechEvent_id] * len(all_data_DatesTime)
    speechEvent_idList = [fullData_multiPhn.speechEvent_id] * len(all_data_DatesTime)
    # create df:
    df = pd.DataFrame.from_dict({dataColName: all_data,
                                 datesColName: all_data_DatesTime,
                                 "triphon": triphonList,
                                 "triphonID": speechEvent_idList})

    # sort df date:
    df = df.sort_values(by=datesColName)

    return df


def createMultiClinicalInfoDF(fullData_multiPhn, clinicalInfoStates: list = ["dry", "unknown", "wet"]):
    dict = {}
    for cis in clinicalInfoStates:
        dict[cis] = createClinicalInfoDF(clinicalInfoState=cis, fullData_multiPhn=fullData_multiPhn)

    return dict


def TagTrainingTestData(all_dry_data_df):
    # TODO: change all_dry_data_df["isTrain"] to use iloc/loc
    # create training and test data:
    all_dry_data_df["isTrain"] = [None] * len(all_dry_data_df)
    i = 0
    while i < len(all_dry_data_df):
        # print(str(i / len(all_dry_data_df))) # debug
        numOfCurrId = len(all_dry_data_df[all_dry_data_df.triphonID == all_dry_data_df.triphonID[i]])
        numOfCurrId_trining = math.floor(numOfCurrId * 0.9)
        numOfCurrId_test = math.ceil(numOfCurrId * 0.1)
        isTrainIdx = list(all_dry_data_df.columns).index("isTrain")
        j = i
        while j < i + numOfCurrId_trining:
            # print("in1: " + str(j / len(all_dry_data_df))) # debug
            all_dry_data_df.iloc[j, isTrainIdx] = True
            j = j + 1
        i = j
        # j = i
        while j < i + numOfCurrId_test:
            # print("in2: " + str(j / len(all_dry_data_df))) # debug
            all_dry_data_df.iloc[j, isTrainIdx] = False
            j = j + 1
        i = j

    # sort df's by triphon and then by date:
    all_dry_data_df = all_dry_data_df.sort_values(["triphonID", "dry_DatesTime"],
                                                  ascending=(True, True)).reset_index(drop=True)

    return all_dry_data_df


def unpackDataToDF(df_dictList):
    # unpack data into df:
    all_dry_data_df = pd.DataFrame(columns=list(df_dictList[0]["dry"].columns))
    all_unknown_data_df = pd.DataFrame(columns=list(df_dictList[0]["unknown"].columns))
    all_wet_data_df = pd.DataFrame(columns=list(df_dictList[0]["wet"].columns))
    for i in range(len(df_dictList)):
        all_dry_data_df = pd.concat([all_dry_data_df, df_dictList[i]["dry"]], axis=0)
        all_unknown_data_df = pd.concat([all_unknown_data_df, df_dictList[i]["unknown"]], axis=0)
        all_wet_data_df = pd.concat([all_wet_data_df, df_dictList[i]["wet"]], axis=0)
    # reset index:
    all_dry_data_df = all_dry_data_df.reset_index(drop=True)
    all_unknown_data_df = all_unknown_data_df.reset_index(drop=True)
    all_wet_data_df = all_wet_data_df.reset_index(drop=True)
    # convert triphoneID's to string if needed:
    for df in [all_dry_data_df, all_unknown_data_df, all_wet_data_df]:
        colIdx = df.columns.get_loc("triphonID")
        for i in range(len(df)):
            if not isinstance(df.triphonID.iloc[i], str):
                df.iloc[i, colIdx] = str(df.iloc[i, colIdx])
    # sort df's by triphon and then by date:
    all_dry_data_df = all_dry_data_df.sort_values(["triphonID", "dry_DatesTime"], ascending=(True, True)).reset_index(
        drop=True)
    all_unknown_data_df = all_unknown_data_df.sort_values(["triphonID", "unknown_DatesTime"],
                                                          ascending=(True, True)).reset_index(drop=True)
    all_wet_data_df = all_wet_data_df.sort_values(["triphonID", "wet_DatesTime"], ascending=(True, True)).reset_index(
        drop=True)
    # drop irrelevant columns:
    if 'Unnamed: 0' in list(all_dry_data_df.columns): all_dry_data_df = all_dry_data_df.drop('Unnamed: 0',
                                                                                             axis=1).reset_index(
        drop=True)
    if 'Unnamed: 0' in list(all_unknown_data_df.columns): all_unknown_data_df = all_unknown_data_df.drop('Unnamed: 0',
                                                                                                         axis=1).reset_index(
        drop=True)
    if 'Unnamed: 0' in list(all_wet_data_df.columns): all_wet_data_df = all_wet_data_df.drop('Unnamed: 0',
                                                                                             axis=1).reset_index(
        drop=True)
    # remove irrelevant data past 31/12/2019:
    date = str(pd.Timestamp(year=2019, month=12, day=31))
    all_dry_data_df = all_dry_data_df[all_dry_data_df.dry_DatesTime <= date].reset_index(drop=True)
    all_unknown_data_df = all_unknown_data_df[all_unknown_data_df.unknown_DatesTime <= date].reset_index(drop=True)
    all_wet_data_df = all_wet_data_df[all_wet_data_df.wet_DatesTime <= date].reset_index(drop=True)
    # create training and test data:
    all_dry_data_df = TagTrainingTestData(all_dry_data_df)

    return all_dry_data_df, all_unknown_data_df, all_wet_data_df


def getTrainingDataPerTriphonSet(all_dry_data_df, all_unknown_data_df, all_wet_data_df, triphonIdSet):
    # filtering df by triphone set:
    if not isinstance(triphonIdSet[0], str):  triphonIdSet = str(triphonIdSet)
    if not isinstance(triphonIdSet, list):  triphonIdSet = [triphonIdSet]
    all_dry_data_df = all_dry_data_df[all_dry_data_df.triphonID.isin(triphonIdSet)].reset_index(drop=True)
    all_unknown_data_df = all_unknown_data_df[all_unknown_data_df.triphonID.isin(triphonIdSet)].reset_index(drop=True)
    all_wet_data_df = all_wet_data_df[all_wet_data_df.triphonID.isin(triphonIdSet)].reset_index(drop=True)
    # unpack tensor data:
    train_dry_data_tensor = torch.tensor(
        all_dry_data_df.drytensor_data[all_dry_data_df.isTrain == True].reset_index(drop=True))
    test_dry_data_tensor = torch.tensor(
        all_dry_data_df.drytensor_data[all_dry_data_df.isTrain == False].reset_index(drop=True))
    # allDryData_tensor = torch.tensor(all_dry_data_df.drytensor_data)
    # allUnknownData_tensor = torch.tensor(all_unknown_data_df.unknowntensor_data)
    # allWetData_tensor = torch.tensor(all_wet_data_df.wettensor_data)
    # reshape tensor data:
    train_dry_data_tensor = torch.reshape(train_dry_data_tensor, (-1, 1, chosenPhnLen, featureFrame_size))
    # test_dry_data_tensor = torch.reshape(test_dry_data_tensor, (-1, 1, chosenPhnLen, featureFrame_size))
    # allDryData_tensor = torch.reshape(allDryData_tensor, (-1, 1, chosenPhnLen, featureFrame_size))
    # allUnknownData_tensor = torch.reshape(allUnknownData_tensor, (-1, 1, chosenPhnLen, featureFrame_size))
    # allWetData_tensor = torch.reshape(allWetData_tensor, (-1, 1, chosenPhnLen, featureFrame_size))
    # unpack datetime data:
    # train_dry_data_dt = all_dry_data_df.dry_DatesTime[all_dry_data_df.isTrain == True]
    # test_dry_data_dt = all_dry_data_df.dry_DatesTime[all_dry_data_df.isTrain == False]
    # allDryData_dt = all_dry_data_df.dry_DatesTime
    # allUnknownData_dt = all_unknown_data_df.unknown_DatesTime
    # allWetData_dt = all_wet_data_df.wet_DatesTime

    return train_dry_data_tensor


def getTestingDataPerTriphon(all_dry_data_df, all_unknown_data_df, all_wet_data_df, triphonesBank, speechEventIdBank,
                             chosenPhnLen, featureFrame_size):
    # filtering df by triphone set:
    train_dry_data_df = all_dry_data_df[all_dry_data_df.isTrain == True].reset_index(drop=True)
    test_dry_data_df = all_dry_data_df[all_dry_data_df.isTrain == False].reset_index(drop=True)
    test_data_dict = {}
    for triphone, speechEventId in zip(triphonesBank, speechEventIdBank):
        str_speechEventId = str(speechEventId)
        test_data_dict[str_speechEventId] = {}
        test_data_dict[str_speechEventId]["dry_train"] = torch.tensor(
            train_dry_data_df.drytensor_data[train_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_data_dict[str_speechEventId]["dry_test"] = torch.tensor(
            test_dry_data_df.drytensor_data[test_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_data_dict[str_speechEventId]["dry"] = torch.tensor(
            all_dry_data_df.drytensor_data[all_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_data_dict[str_speechEventId]["unknown"] = torch.tensor(
            all_unknown_data_df.unknowntensor_data[all_unknown_data_df.triphonID == str_speechEventId].reset_index(
                drop=True))
        test_data_dict[str_speechEventId]["wet"] = torch.tensor(
            all_wet_data_df.wettensor_data[all_wet_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_data_dict[str_speechEventId]["all"] = torch.cat((torch.tensor(
            all_dry_data_df.drytensor_data[all_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True)),
                                                               torch.tensor(all_unknown_data_df.unknowntensor_data[
                                                                                all_unknown_data_df.triphonID == str_speechEventId].reset_index(
                                                                   drop=True)),
                                                               torch.tensor(all_wet_data_df.wettensor_data[
                                                                                all_wet_data_df.triphonID == str_speechEventId].reset_index(
                                                                   drop=True))), 0)

    # reshape tensor data:
    clinicalInfoLabels = ["dry_train", "dry_test", "dry", "unknown", "wet", "all"]
    for speechEventId in speechEventIdBank:
        str_speechEventId = str(speechEventId)
        for ci in clinicalInfoLabels:
            test_data_dict[str_speechEventId][ci] = torch.reshape(test_data_dict[str_speechEventId][ci],
                                                                   (-1, 1, chosenPhnLen, featureFrame_size))

    test_dt_dict = {}
    for triphone, speechEventId in zip(triphonesBank, speechEventIdBank):
        str_speechEventId = str(speechEventId)
        test_dt_dict[str_speechEventId] = {}
        test_dt_dict[str_speechEventId]["dry_train"] = list(
            train_dry_data_df.dry_DatesTime[train_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_dt_dict[str_speechEventId]["dry_test"] = list(
            test_dry_data_df.dry_DatesTime[test_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_dt_dict[str_speechEventId]["dry"] = list(
            all_dry_data_df.dry_DatesTime[all_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_dt_dict[str_speechEventId]["unknown"] = list(
            all_unknown_data_df.unknown_DatesTime[all_unknown_data_df.triphonID == str_speechEventId].reset_index(
                drop=True))
        test_dt_dict[str_speechEventId]["wet"] = list(
            all_wet_data_df.wet_DatesTime[all_wet_data_df.triphonID == str_speechEventId].reset_index(drop=True))
        test_dt_dict[str_speechEventId]["all"] = list(all_dry_data_df.dry_DatesTime[all_dry_data_df.triphonID == str_speechEventId].reset_index(drop=True)) + \
                                                  list(all_unknown_data_df.unknown_DatesTime[all_unknown_data_df.triphonID == str_speechEventId].reset_index(drop=True)) + \
                                                  list(all_wet_data_df.wet_DatesTime[all_wet_data_df.triphonID == str_speechEventId].reset_index(drop=True))

    return test_data_dict, test_dt_dict

def isUtf8(str):
    valid_utf8 = True
    try:
        str.encode().decode('utf-8')
    except UnicodeDecodeError:
        valid_utf8 = False

    return valid_utf8

def createAllExperimentPhnIdSets_fromLexicon(
        path=r"\\192.168.55.210\f$\Or-rnd\code\Python\CordioAlgorithmsPython\asr\CordioLexiconVariations.csv"):
    lex = Lexicon(LexiconCSV=Path(path), shouldParseAgain=False)
    df = lex.GetLexicon()
    i = 0
    # l_id = df["language_id"][i]
    # s_id = df["sentence"][i]
    # w_id = df["word"][i]
    # p_id = df["phoneme"][i]
    # id = l_id * 1e3 + s_id * 1e2 + w_id * 1e1 + p_id
    # fullIdList = [int(id)]
    p1 = "sil"
    p2 = df["phoneme"][i]
    p3 = df["phoneme"][i + 1]
    phn = p1 + "-" + p2 + "+" + p3
    triphonList = [phn]

    for i in range(1, len(df) - 1):
        p1 = df["phoneme"][i - 1]
        p2 = df["phoneme"][i]
        p3 = df["phoneme"][i + 1]
        # if p1 == "��": p1 = "sil"
        # if p2 == "��": continue
        # if p3 == "��": p3 = "sil"
        if not p1.isalpha(): p1 = "sil"
        if not p2.isalpha(): continue
        if not p3.isalpha(): p3 = "sil"
        phn = p1 + "-" + p2 + "+" + p3
        triphonList.append(phn)
        # l_id = df["language_id"][i]
        # s_id = df["sentence"][i]
        # w_id = df["word"][i]
        # p_id = df["phoneme"][i]
        # id = l_id * 1e3 + s_id * 1e2 + w_id * 1e1 + p_id
        # fullIdList.append(int(id))


    i = len(df) - 1
    # l_id = df["language_id"][i]
    # s_id = df["sentence"][i]
    # w_id = df["word"][i]
    # p_id = df["phoneme"][i]
    # id = l_id * 1e3 + s_id * 1e2 + w_id * 1e1 + p_id
    # fullIdList.append(int(id))
    p1 = df["phoneme"][i - 1]
    p2 = df["phoneme"][i]
    p3 = "sil"
    phn = p1 + "-" + p2 + "+" + p3
    triphonList.append(phn)

    # remove non ascii triphones:
    mask = [True] * len(triphonList)
    for i in range(len(triphonList)):
        if 'ʔ' in triphonList[i]: mask[i] = False
    fullIdList = np.array(df["phoneme_id"])[mask].tolist()
    triphonList = np.array(triphonList)[mask].tolist()

    return fullIdList, triphonList

def createChosenLenPerPhoneme(speechEventIdBank, triphonesBank, feature, savePath, windoweLen=20e-3, stride=5e-3,
                              jobsNum=1):
    if Path(savePath).is_file():
        df = pd.read_csv(savePath, index_col=False)
    else:
        df = pd.DataFrame(
            columns=["triphones", "phonemeId", "chosenLen", "numberOfSamples", "averageSampleLen", "chosenLen seconds",
                     "averageSampleLen_seconds"])

    # remove unwanted columns:
    if 'Unnamed: 0' in df.columns: df.drop('Unnamed: 0', inplace=True, axis=1)
    fullData_multiPhn_triphone = preprocessData_multiTriphon(patient=patient, speechEvent_id=speechEventIdBank,
                                                             dbPath=dbPath, jobsNum=jobsNum, feature=feature,
                                                             se_triphon=triphonesBank, saveLocationUrl=saveLocationUrl)
    chosenLen = fullData_multiPhn_triphone.chosenPhnLen
    # add data to csv:
    df = df.append({"triphones": str(triphonesBank),
                    "phonemeId": str(speechEventIdBank),
                    "chosenLen": chosenLen,
                    "numberOfSamples": len(fullData_multiPhn_triphone.FeatureFrames_df.file),
                    "averageSampleLen": round(
                        fullData_multiPhn_triphone.FeatureFrames_df.mfcc.apply(lambda x: len(x)).mean()),
                    "chosenLen seconds": chosenLen * (windoweLen - stride) + stride,
                    "averageSampleLen_seconds": fullData_multiPhn_triphone.FeatureFrames_df.mfcc.apply(
                        lambda x: len(x)).apply(lambda y: y * (windoweLen - stride) + stride).mean()},
                   ignore_index=True)
    df.reset_index(drop=True, inplace=True)

    # save as CSV:
    df.to_csv(savePath, index=False)

    return df

# training functions:

def getTrainedModelAndLoss(netModel, AE_Net_manual_weight_init, optim, train_loader, save_location_path, model_name,
                           epochs=350, forceModelTrain=False):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # create a model from `AE` autoencoder class
    # load it to the specified device, either gpu or cpu
    model = netModel().to(device)

    if not Path.is_file(save_location_path / model_name) or forceModelTrain:
        # initializing model:
        # --------------------

        model.apply(AE_Net_manual_weight_init)

        # create an optimizer object
        # Adam optimizer with learning rate 1e-3
        LR = 1e-2
        optimizer = optim.Adam(model.parameters(), lr=LR)
        # mean-squared error loss
        criterion = nn.MSELoss()

        model, lossList = AE_train(epochs, train_loader, chosenPhnLen, featureFrame_size, device, model, optimizer,
                                   criterion, save_location_path=save_location_path, modelName=model_name,
                                   toleranceParcent=0.000005, toleranceLen=4)
    else:
        model.load_state_dict(torch.load(str(save_location_path / model_name)))
        listName = str(Path(model_name).stem) + '_lossList.pt'
        lossList = torch.load(str(save_location_path / Path(listName)))

    return model, lossList

# testing functions:

def getPreTestData(testLoss, test_dataset_DateTime, test_dataset_labels, tools):
    df = pd.DataFrame({"Date": test_dataset_DateTime, "TestLoss": testLoss})
    dateIdx = df.columns.get_loc("Date")
    for i in range(len(df)):
        df.iloc[i, dateIdx] = df.iloc[i, dateIdx].date()
    if testLoss == []: return df
    # converting clinical info labels to num:
    start = min(testLoss)
    stop = max(testLoss)
    clinicalInfoNums = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
    clinicalInfo2Num = {'dry_training': clinicalInfoNums[0],
                        'dry': clinicalInfoNums[1],
                        'unknown': clinicalInfoNums[2],
                        'wet': clinicalInfoNums[3]}
    test_dataset_labels_num = []
    for test_dataset_label in test_dataset_labels:
        test_dataset_labels_num.append(clinicalInfo2Num[test_dataset_label])
    df["clinicalInfoNum"] = test_dataset_labels_num
    sort_df = df.sort_values('Date')
    df_clean = tools.averageDataInSameDay(sort_df, "Date")

    # setting clinical info of mixed clinical info days into one(maybe bad to do):
    clinicalInfoIdx = df.columns.get_loc("clinicalInfoNum")
    for i in range(len(df_clean)):
        if df_clean["clinicalInfoNum"][i] not in clinicalInfoNums:
            if pd.isnull(df_clean["clinicalInfoNum"][i]):
                continue
            elif i == 0:
                df_clean.iloc["clinicalInfoNum"][i] = df_clean.iloc["clinicalInfoNum"][i]
            else:
                df_clean.iloc["clinicalInfoNum"][i] = df_clean.iloc["clinicalInfoNum"][i]

    # scaling clinical info num:
    start = df_clean["TestLoss"].min()
    stop = df_clean["TestLoss"].max()
    clinicalInfoNumsScale = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
    clinicalInfoscale = {clinicalInfoNums[0]: clinicalInfoNumsScale[0],
                         clinicalInfoNums[1]: clinicalInfoNumsScale[1],
                         clinicalInfoNums[2]: clinicalInfoNumsScale[2],
                         clinicalInfoNums[3]: clinicalInfoNumsScale[3]}
    clinicalInfoScaleLabel = {clinicalInfoNumsScale[0]: "dry_training",
                              clinicalInfoNumsScale[1]: "dry",
                              clinicalInfoNumsScale[2]: "unknown",
                              clinicalInfoNumsScale[3]: "wet"}
    invClinicalInfoScaleLabel = {v: k for k, v in clinicalInfoScaleLabel.items()}
    for i in range(len(df_clean)):
        if pd.isnull(df_clean["clinicalInfoNum"][i]):
            continue
        else:
            df_clean["clinicalInfoNum"][i] = clinicalInfoscale[df_clean["clinicalInfoNum"][i]]

    return df_clean


def getModelTestData_df(speechEventIdBank, triphonesBank, test_dt_dict, model, disablePbar=False):
    firstLoopFlag = True
    pbar = tqdm(range(len(speechEventIdBank)), desc="creating Test Data...", disable=disablePbar)
    for speechEventId, triphone, i in zip(speechEventIdBank, triphonesBank, pbar):  # speech event
        test_dataset_DateTime = test_dt_dict[str(speechEventId)]["all"]
        # test_dataset_DateTime = [d.date() for d in test_dataset_DateTime]
        currTestloader = test_dataLoader_tensor_sets[str(speechEventId)]
        if len(currTestloader) < 10:
            continue
        # if not currTestloader: continue
        testLoss = AE_test(model, currTestloader, chosenPhnLen, featureFrame_size, device, criterion)
        tools = Tools()
        test_dataset_labels = ['dry_training'] * len(test_data_dict[str(speechEventId)]["dry_train"]) + \
                              ['dry'] * len(test_data_dict[str(speechEventId)]["dry_test"]) + \
                              ['unknown'] * len(test_data_dict[str(speechEventId)]["unknown"]) + \
                              ['wet'] * len(test_data_dict[str(speechEventId)]["wet"])
        SE_df = getPreTestData(testLoss, test_dataset_DateTime, test_dataset_labels, tools)
        if firstLoopFlag:
            SE_df = SE_df.rename(columns={'TestLoss': 'TestLoss_' + triphone})
            if 'clinicalInfoNum' in list(SE_df.columns):
                SE_df = SE_df.drop(columns='clinicalInfoNum')
            SE_test_df = SE_df
        else:
            SE_test_df["TestLoss_" + triphone] = SE_df['TestLoss']
        firstLoopFlag = False

    SE_test_df['clinicalInfoNum'] = SE_df['clinicalInfoNum']
    return SE_test_df


def test(testLoss, test_dataset_DateTime, test_dataset_labels, saveURL=None, y_title="MSE Error", colorCIGraph=False,
         modelStr=None):
    df = pd.DataFrame({"Date": test_dataset_DateTime, "TestLoss": testLoss})
    for i in range(len(df)):
        df["Date"][i] = df["Date"][i].date()
    # converting clinical info labels to num:
    start = min(testLoss);
    stop = max(testLoss)
    clinicalInfoNums = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
    clinicalInfo2Num = {'dry_training': clinicalInfoNums[0],
                        'dry': clinicalInfoNums[1],
                        'unknown': clinicalInfoNums[2],
                        'wet': clinicalInfoNums[3]}
    test_dataset_labels_num = []
    for test_dataset_label in test_dataset_labels:
        test_dataset_labels_num.append(clinicalInfo2Num[test_dataset_label])
    df["clinicalInfoNum"] = test_dataset_labels_num
    sort_df = df.sort_values('Date')
    df_clean = tools.averageDataInSameDay(sort_df, "Date")

    # setting clinical info of mixed clinical info days into one(maybe bad to do):
    for i in range(len(df_clean)):
        if df_clean["clinicalInfoNum"][i] not in clinicalInfoNums:
            if pd.isnull(df_clean["clinicalInfoNum"][i]):
                continue
            elif i == 0:
                df_clean["clinicalInfoNum"][i] = df_clean["clinicalInfoNum"][i + 1]
            else:
                df_clean["clinicalInfoNum"][i] = df_clean["clinicalInfoNum"][i - 1]

    # scaling clinical info num:
    start = df_clean["TestLoss"].min()
    stop = df_clean["TestLoss"].max()
    clinicalInfoNumsScale = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
    clinicalInfoscale = {clinicalInfoNums[0]: clinicalInfoNumsScale[0],
                         clinicalInfoNums[1]: clinicalInfoNumsScale[1],
                         clinicalInfoNums[2]: clinicalInfoNumsScale[2],
                         clinicalInfoNums[3]: clinicalInfoNumsScale[3]}
    clinicalInfoScaleLabel = {clinicalInfoNumsScale[0]: "dry_training",
                              clinicalInfoNumsScale[1]: "dry",
                              clinicalInfoNumsScale[2]: "unknown",
                              clinicalInfoNumsScale[3]: "wet"}
    invClinicalInfoScaleLabel = {v: k for k, v in clinicalInfoScaleLabel.items()}
    for i in range(len(df_clean)):
        if pd.isnull(df_clean["clinicalInfoNum"][i]):
            continue
        else:
            df_clean["clinicalInfoNum"][i] = clinicalInfoscale[df_clean["clinicalInfoNum"][i]]
    # plot:
    f, ax = plt.subplots(figsize=(15, 7.5), dpi=160)
    ax = df_clean.plot(grid=True, ax=ax)
    if colorCIGraph:
        colors = ["green", "limegreen", "grey", "steelblue"]
        for cs, color in zip(list(invClinicalInfoScaleLabel.keys()), colors):
            tmp_df_cs_dates = (df_clean[df_clean["clinicalInfoNum"] == invClinicalInfoScaleLabel[cs]]).index
            # tmp_df_cs_dates = tmp_df_cs_dates.to_pydatetime()
            if list(tmp_df_cs_dates) == []: continue
            csStartDate = tmp_df_cs_dates[0]
            csEndDate = tmp_df_cs_dates[-1]
            plt.axvspan(csStartDate, csEndDate, label=cs, color=color, alpha=0.3)
    ax.legend()
    ax.set_ylabel(y_title)
    # set title:
    if modelStr is None:
        title = patient + ' test'
    else:
        title = 'Model ' + modelStr + ': ' + patient + ' test'
    if "normalized" in saveURL: title = title + "\n normalized data"
    ax.set_title(title, size=18)

    locks = ax.get_yticks()
    locks = list(locks)
    for cs in list(invClinicalInfoScaleLabel.keys()):
        locks.append(invClinicalInfoScaleLabel[cs])
    locks.sort()
    newLabels = []
    for loc in locks:
        if loc in list(clinicalInfoScaleLabel.keys()):
            ap = clinicalInfoScaleLabel[loc]
            newLabels.append(ap + "->       ")
        else:
            newLabels.append("%.2f" % loc)
    # plt.yticks([])
    ax.set_yticks(locks)
    ax.set_yticklabels(list(OrderedDict.fromkeys(newLabels)))

    if saveURL is not None:
        Path(saveURL).parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(saveURL + ".png")
    plt.show()


def testNsave(save_location_path, model_name, normlizeDataBeforeTraining, speechEvent, criterion, test_dataset_labels,
              test_dataset_DateTime, chosenPhnLen, test_loader):
    if normlizeDataBeforeTraining:
        MSE_testSaveDataName = str(save_location_path / Path("data") / Path(
            model_name[:-7])) + "_" + speechEvent + "_" + speechEvent + "_" + criterion._get_name() + "_test_figure"
        saveGraphURL = str(save_location_path / Path("graphs") / Path(
            model_name[:-7])) + "_" + speechEvent + "_" + criterion._get_name() + "_normalizedtrainData_test_figure"
    else:
        MSE_testSaveDataName = str(save_location_path / Path("data") / Path(
            model_name[:-7])) + "_" + speechEvent + "_" + criterion._get_name() + "_test_figure"
        saveGraphURL = str(save_location_path / Path("graphs") / Path(
            model_name[:-7])) + "_" + speechEvent + "_" + criterion._get_name() + "_test_figure"

    if Path.exists(Path(MSE_testSaveDataName)):
        # load test data:
        with open(MSE_testSaveDataName, "rb") as fp:  # Unpickling
            testLoss = pickle.load(fp)
    else:
        testLoss = AE_test(model, test_loader, chosenPhnLen, featureFrame_size, device, criterion,
                           model.fc1.in_features)
        # save test data:
        Path(MSE_testSaveDataName).parent.mkdir(parents=True, exist_ok=True)
        with open(MSE_testSaveDataName, "wb") as fp:  # Pickling
            pickle.dump(testLoss, fp)

    test(testLoss, test_dataset_DateTime, test_dataset_labels, saveURL=saveGraphURL, y_title=criterion._get_name())


def getCurollation(patient, dts, testData):
    # get corelation scores:
    # Extract production graph data and datetime array
    # dts, Gprod = your_function_to_load_production_graph()
    # create theoretical clean graph
    clinical_status = CordioClinicalInformation(patient)
    Gclinical = clinical_status(dts)
    # Gclinical = clinical_status(dts.tolist())
    # Get correlation score
    distance_correlation = CordioEvalGraphCorrelation(dts, testData, Gclinical)

    return distance_correlation


def plotNsave(testData_df, allCorrelationScores, testPlotSaveUrl, invClinicalInfoScaleLabel, show=True):
    str_triphoneSet = setStrSpeechEventSet(triphoneSet)
    f, ax = plt.subplots(figsize=(15, 7.5), dpi=300)
    ax = testData_df.plot(grid=True, ax=ax, linestyle='-')
    if colorCIGraph:
        colors = ["green", "limegreen", "grey", "steelblue"]
        for cs, color in zip(list(invClinicalInfoScaleLabel.keys()), colors):
            tmp_df_cs_dates = (testData_df[testData_df["clinicalInfoNum"] == invClinicalInfoScaleLabel[cs]]).index
            # tmp_df_cs_dates = tmp_df_cs_dates.to_pydatetime()
            if list(tmp_df_cs_dates) == []: continue
            csStartDate = tmp_df_cs_dates[0]
            csEndDate = tmp_df_cs_dates[-1]
            plt.axvspan(csStartDate, csEndDate, label=cs, color=color, alpha=0.1)
    legendList = testData_df.columns[0:-1]
    if len(legendList) > 7:
        maxKeys = heapq.nlargest(6, allCorrelationScores, key=allCorrelationScores.get)
        legendList = [s + ', curr=' + str(allCorrelationScores[s.split("_")[-1]])[0:7] for s in legendList if s.split("_")[-1] in maxKeys]
    else:
        legendList = [s + ', curr=' + str(allCorrelationScores[s.split("_")[-1]])[0:7] for s in legendList]
    ax.legend(legendList)
    ax.set_ylabel(y_title)
    # set title:
    if modelStr is None:
        title = patient + ' test\n' + str_triphoneSet
    else:
        title = 'Model ' + modelStr + ': ' + patient + ' test \n' + str_triphoneSet
    if "normalized" in testPlotSaveUrl: title = title + "\n normalized_data"
    ax.set_title(title, size=18)

    locks = ax.get_yticks()
    locks = list(locks)
    for cs in list(invClinicalInfoScaleLabel.keys()):
        locks.append(invClinicalInfoScaleLabel[cs])
    locks.sort()
    newLabels = []
    for loc in locks:
        if loc in list(clinicalInfoScaleLabel.keys()):
            ap = clinicalInfoScaleLabel[loc]
            newLabels.append(ap + "->       ")
        else:
            newLabels.append("%.2f" % loc)
    # plt.yticks([])
    ax.set_yticks(locks)
    ax.set_yticklabels(list(OrderedDict.fromkeys(newLabels)))

    if testPlotSaveUrl is not None:
        Path(testPlotSaveUrl).parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(testPlotSaveUrl)
    if show: plt.show()
    plt.close()

def setStrSpeechEventSet(SE: Union[str, list]):
    if isinstance(SE, str):
        SE = ast.literal_eval(SE)
    if isinstance(SE, list):
        if len(SE) > 7: str_se = str(SE[0:7])[0:-1] + ", more_]"
        else: str_se = str(speechEventIdBank)
    return str_se


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------getting data ready------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

colorCIGraph = True
patient = "NHR-0001"  # "BLN-0001"
speechEvent_id = ["i", "o"]
se_triphon = ["m-i+n", "d-o+m"]
if platform.node() == 'RDSRV': dbPath = r"F:\db"
else: dbPath = r"\\192.168.55.210\db" #r"E:\Or_rnd\db"
if platform.node() == 'RDSRV': saveLocationUrl = r"F:\Or-rnd\Autoencoder_project\dump\data"
else: saveLocationUrl = r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump\data"
saveName = patient + "_" + str(speechEvent_id) + "_mfcc_framesBySe_df"
normlizeDataBeforeTraining = True
shuffle = False
fullData_multiPhn_jobsNum = 8
jobsNum = 1
forceReCulc = False
forceModelTrain = False  # froce training the model and overwrite it in path
feature = "mfcc"
# trianBatch_size = 155
if feature == "mfcc":
    featureFrame_size = 12
else:
    featureFrame_size = 1

# triphonesBank = ["m-i+n", "d-o+m", "h-a+d", "f-e+k", "sil-e+f", "n-o+sil"]
# triphonesBank = ["m-i+n", "d-o+m", "h-a+d", "f-e+k", "sil-e+f", "n-o+sil"] #"sil-e+f" = "ʔ-e+f" & "h-a+d" = "ʔ-a+d"
# phonemesBank = ["i", "o", "a", "e", "e", "o"]
# speechEventIdBank = [100070207, 100070205, 100070203, 100070104, 100070102, 100070209]
speechEventIdBank, triphonesBank = createAllExperimentPhnIdSets_fromLexicon(path=r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump\data\CordioLexiconForAE.csv")
triphone2speechEventId = dict(zip(triphonesBank, speechEventIdBank))

if len(triphonesBank) > 8: str_triphonesBank = str(triphonesBank[0:7])[0:-1]+", more_]"
else: str_triphonesBank = str(triphonesBank)
if len(speechEventIdBank) > 8: str_speechEventIdBank = str(triphonesBank[0:7])[0:-1]+", more_]"
else: str_speechEventIdBank = str(speechEventIdBank)
chosenLenPerPhonemePath = str(Path(saveLocationUrl) / Path(r"chosenLenPerPhoneme.csv"))

# experimentPhnSets = [["m-i+n"],
#                      ["m-i+n", "d-o+m"],
#                      ["m-i+n", "d-o+m", "h-a+d"],
#                      ["m-i+n", "d-o+m", "h-a+d", "f-e+k"],
#                      ["m-i+n", "d-o+m", "h-a+d", "f-e+k", "sil-e+f"],
#                      ["m-i+n", "d-o+m", "h-a+d", "f-e+k", "sil-e+f", "n-o+sil"]]
# experimentPhnSets = ["m-i+n", "d-o+m", "h-a+d", "f-e+k", "sil-e+f", "n-o+sil"]
# experimentPhnIdSets = [[100070207],
#                        [100070207, 100070205],
#                        [100070207, 100070205, 100070203],
#                        [100070207, 100070205, 100070203, 100070104],
#                        [100070207, 100070205, 100070203, 100070104, 100070102],
#                        [100070207, 100070205, 100070203, 100070104, 100070102, 100070209]]
# experimentPhnIdSets = [100070207, 100070205, 100070203, 100070104, 100070102, 100070209]
# experimentPhnIdSets = [[1111, 1112, 1113, 1114, 1115, 1116, 1117, 1121, 1122, 1123, 1124, 1125, 1131, 1132, 1133, 1134, 1135, 1141, 1142, 1143, 1151, 1152, 1153, 1154, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1211, 1212, 1213, 1214, 1215, 1221, 1222, 1223, 1224, 1225, 1226, 1231, 1232, 1233, 1241, 1242, 1243, 1244, 1245, 1246, 1311, 1312, 1313, 1314, 1315, 1321, 1322, 1323, 1331, 1332, 1333, 1334, 1335, 1336, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1421, 1422, 1423, 1424, 1425, 1431, 1432, 1433, 1434, 1435, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1451, 1452, 1453, 1454, 1455, 1456, 1511, 1512, 1513, 1514, 1515, 1521, 1522, 1523, 1524, 1525, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1611, 1612, 1613, 1614, 1615, 1621, 1622, 1623, 1624, 1625, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1711, 1712, 1713, 1714, 1715, 1716, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1811, 1812, 1813, 1814, 1815, 1821, 1822, 1823, 1824, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1911, 1912, 1913, 1914, 1915, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2111, 2112, 2113, 2114, 2121, 2122, 2123, 2124, 2125, 2126, 2131, 2132, 2133, 2141, 2142, 2143, 2144, 2145, 2146, 2147, 2151, 2152, 2153, 2154, 2161, 2162, 2163, 2164, 2165, 2166, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2221, 2222, 2223, 2224, 2225, 2231, 2232, 2233, 2234, 2235, 2236, 2237, 2241, 2242, 2243, 2244, 2245, 2246, 2247, 2251, 2252, 2253, 2254, 2261, 2262, 2263, 2264, 2311, 2312, 2313, 2314, 2321, 2322, 2323, 2324, 2325, 2331, 2332, 2333, 2334, 2335, 2336, 2341, 2342, 2343, 2344, 2345, 2351, 2352, 2353, 2354, 2355, 2361, 2362, 2363, 2364, 2371, 2372, 2373, 2374, 2375, 2381, 2382, 2383, 2384, 2385, 2411, 2412, 2413, 2414, 2421, 2422, 2423, 2424, 2431, 2432, 2433, 2434, 2441, 2442, 2443, 2444, 2451, 2452, 2453, 2454, 2511, 2512, 2513, 2514, 2521, 2522, 2523, 2524, 2531, 2532, 2533, 2534, 2541, 2542, 2543, 2544, 2551, 2552, 2553, 2554, 2611, 2612, 2613, 2614, 2621, 2622, 2623, 2624, 2625, 2631, 2632, 2633, 2634, 2635, 2636, 2711, 2712, 2713, 2714, 2715, 2721, 2722, 2723, 2724, 2725, 2731, 2732, 2733, 2734, 2735, 2741, 2742, 2743, 2744, 2745, 2751, 2752, 2753, 2754, 2755, 2811, 2812, 2813, 2814, 2815, 2821, 2822, 2823, 2824, 2831, 2832, 2833, 2841, 2842, 2843, 2844, 2851, 2852, 2853, 2854, 2861, 2862, 2863, 2871, 2872, 2873, 2874, 2881, 2882, 2883, 2884, 2891, 2892, 2893, 2901, 2902, 2903, 2904, 2911, 2912, 2913, 2914, 2915, 2921, 2922, 2923, 2924, 2925, 2116, 2111, 2112, 2113, 2114, 2115, 2121, 2122, 2123, 2124, 2125, 2131, 2132, 2133, 2134, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 2221, 2222, 2223, 2221, 2222, 2223, 2224, 2225, 2226, 2227, 2228, 2229, 2230, 2231, 2311, 2312, 2313, 2314, 2315, 2316, 2317, 2321, 2322, 2323, 2324, 2325, 2326, 2411, 2412, 2413, 2414, 2415, 2421, 2422, 2423, 2424, 2425, 2431, 2432, 2433, 2434, 2435, 2436, 2511, 2512, 2513, 2514, 2515, 2516, 2517, 2518, 2521, 2522, 2523, 2524, 2525, 2526, 2531, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2541, 2542, 2543, 2544, 2545, 2546, 2547, 2548, 2549, 2611, 2612, 2613, 2614, 2621, 2622, 2623, 2624, 2625, 2626, 2627, 2628, 2631, 2632, 2633, 2634, 2635, 2636, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2721, 2722, 2723, 2724, 2725, 2726, 2727, 2731, 2732, 2733, 2734, 2735, 2811, 2812, 2813, 2814, 2821, 2822, 2823, 2824, 2825, 2826, 2831, 2832, 2833, 2834, 2835, 2841, 2842, 2843, 2844, 2845, 2846, 2911, 2912, 2913, 2914, 2921, 2922, 2923, 2924, 2925, 2926, 2931, 2932, 2941, 2942, 2943, 2944, 2945, 2946, 3011, 3012, 3013, 3014, 3021, 3022, 3023, 3034, 3035, 3036, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3111, 3112, 3113, 3114, 3115, 3121, 3122, 3123, 3124, 3125, 3131, 3132, 3141, 3142, 3151, 3152, 3153, 3154, 3161, 3162, 3163, 3164, 3165, 3211, 3212, 3213, 3214, 3221, 3222, 3223, 3224, 3231, 3232, 3233, 3234, 3241, 3242, 3243, 3244, 3251, 3252, 3253, 3254, 3261, 3262, 3263, 3264, 3271, 3272, 3273, 3274, 3281, 3282, 3283, 3284, 3291, 3292, 3293, 3294, 3301, 3302, 3303, 3304, 3311, 3312, 3313, 3314, 3321, 3322, 3323, 3324, 3611, 3612, 3621, 3622, 3623, 3624, 3631, 3632, 3633, 3641, 3642, 3651, 3652, 3661, 3662, 3663, 3711, 3712, 3713, 3721, 3722, 3723, 3731, 3732, 3741, 3751, 3752, 3753, 3761, 3762, 3763, 3811, 3812, 3813, 3821, 3822, 3831, 3832, 3833, 3911, 3912, 3921, 3922, 3923, 3931, 3932, 3933, 3941, 3942, 3943, 3951, 3961, 3962, 3963, 3964, 3965, 4011, 4012, 4013, 4021, 4022, 4023, 4031, 4041, 4042, 4043, 4044, 4051, 4052, 4053, 4061, 4062, 4071, 4072, 4111, 4112, 4113, 4121, 4122, 4123, 4124, 4131, 4132, 4133, 4141, 4142, 4143, 4144, 4151, 4152, 4153, 4161, 4162, 4163, 4164, 4165, 4171, 4172, 4173, 4174, 4175, 4176, 4181, 4182, 4183, 4184, 4185, 4186, 4191, 4192, 4193, 4201, 4202, 4203, 4204, 4211, 4212, 4213, 4214, 4111, 4112, 4113, 4114, 4115, 4121, 4122, 4123, 4124, 4125, 4131, 4132, 4133, 4134, 4135, 4141, 4142, 4143, 4144, 4151, 4152, 4153, 4154, 4161, 4162, 4163, 4171, 4172, 4173, 4174, 4175, 4211, 4212, 4213, 4214, 4221, 4222, 4223, 4224, 4225, 4226, 4231, 4232, 4233, 4311, 4312, 4313, 4314, 4315, 4316, 4321, 4322, 4323, 4325, 4326, 4327, 4328, 4329, 4331, 4332, 4333, 4334, 4335, 4336, 4337, 4338, 4339, 4340, 4411, 4412, 4421, 4422, 4423, 4424, 4425, 4431, 4432, 4433, 4434, 4435, 4436, 4437, 4441, 4442, 4443, 4444, 4445, 4446, 4447, 4511, 4512, 4513, 4514, 4515, 4516, 4517, 4521, 4522, 4523, 4524, 4525, 4531, 4532, 4533, 4534, 4535, 4536, 4537, 4538, 4539, 4540, 4541, 4541, 4542, 4543, 4544, 4545, 4546, 4547, 4548, 4549, 4611, 4612, 4613, 4614, 4615, 4621, 4622, 4623, 4624, 4631, 4632, 4633, 4634, 4635, 4641, 4642, 4643, 4644, 4645, 4711, 4712, 4713, 4714, 4721, 4722, 4723, 4724, 4731, 4732, 4733, 4734, 4811, 4812, 4813, 4814, 4815, 4816, 4821, 4822, 4823, 4824, 4825, 4831, 4832, 4833, 4911, 4912, 4913, 4914, 4915, 4921, 4922, 4923, 4924, 4925, 4931, 4932, 4933, 4934, 4935, 5011, 5012, 5013, 5014, 5015, 5021, 5022, 5023, 5024, 5031, 5032, 5033, 5034, 5035, 5111, 5112, 5113, 5114, 5115, 5116, 5117, 5121, 5122, 5123, 5124, 5125, 5131, 5132, 5133, 5134, 5135, 5136, 5141, 5142, 5143, 5144, 5145, 5211, 5212, 5213, 5214, 5221, 5222, 5223, 5224, 5231, 5232, 5233, 5234, 5235, 5241, 5242, 5243, 5244, 5245, 5251, 5252, 5253, 5254, 5255, 5256, 5261, 5262, 5263, 5264, 5265, 5266, 5271, 5272, 5273, 5274, 5281, 5282, 5283, 5284, 5285, 5286, 5291, 5292, 5293, 5294, 5301, 5302, 5303, 5304, 5305, 5306, 5311, 5312, 5313, 5314, 5315, 5321, 5322, 5323, 5324, 5325, 5111, 5112, 5113, 5121, 5122, 5123, 5131, 5132, 5133, 5141, 5142, 5143, 5144, 5145, 5146, 5151, 5152, 5153, 5154, 5155, 5156, 5157, 5161, 5162, 5163, 5164, 5165, 5166, 5167, 5168, 5211, 5212, 5214, 5215, 5216, 5221, 5222, 5223, 5224, 5225, 5231, 5232, 5241, 5242, 5243, 5244, 5245, 5246, 5311, 5312, 5313, 5314, 5315, 5321, 5322, 5324, 5325, 5326, 5327, 5328, 5331, 5332, 5333, 5334, 5335, 5341, 5342, 5343, 5344, 5345, 5351, 5352, 5361, 5362, 5371, 5372, 5373, 5374, 5381, 5382, 5391, 5392, 5393, 5394, 5395, 5396, 5411, 5412, 5413, 5414, 5421, 5422, 5431, 5432, 5433, 5434, 5435, 5436, 5441, 5442, 5443, 5444, 5511, 5512, 5513, 5514, 5521, 5522, 5523, 5524, 5525, 5526, 5527, 5531, 5532, 5533, 5534, 5541, 5542, 5543, 5544, 5551, 5552, 5561, 5562, 5563, 5564, 5565, 5566, 5567]]

experimentPhnIdSets, experimentPhnSets = [speechEventIdBank], [triphonesBank]
for seID, phn in zip(speechEventIdBank, triphonesBank):
    experimentPhnIdSets.append([seID])
    experimentPhnSets.append([phn])

if __name__ == '__main__':

    # ------------------------------------------------------------------------------------------------------------------
    # ----------------------------------------------create training and testing data------------------------------------
    # ------------------------------------------------------------------------------------------------------------------

    # create fullData objects for each dataset:
    # fullData_multiPhn_triphoneList = [preprocessData_multiTriphon(patient=patient, speechEvent_id=speechEventID,
    #                                                               dbPath=dbPath, saveLocationUrl=saveLocationUrl,
    #                                                               saveName=saveName, jobsNum=fullData_multiPhn_jobsNum,
    #                                                               forceReCulc=forceReCulc, feature=feature,
    #                                                               se_triphon=triphone, turnOffProgBars=True,
    #                                                               disablePrints=True)
    #                                   for speechEventID, triphone in zip(speechEventIdBank, triphonesBank)]
    fullData_multiPhn_triphoneList = [preprocessData_multiTriphon(patient=patient, speechEvent_id=speechEventID,
                                                                  dbPath=dbPath, saveLocationUrl=saveLocationUrl,
                                                                  saveName=saveName, jobsNum=fullData_multiPhn_jobsNum,
                                                                  forceReCulc=forceReCulc, feature=feature,
                                                                  se_triphon=triphone, turnOffProgBars=True,
                                                                  disablePrints=True)
                                      for speechEventID, triphone in zip(experimentPhnIdSets, experimentPhnSets)]
    # set model input lengths for each dataset:
    if Path(chosenLenPerPhonemePath).is_file():
        chosenPhnLen_df = pd.read_csv(chosenLenPerPhonemePath, engine='python')
        chosenPhnLen_df.drop_duplicates(inplace=True)
        if not any(str(speechEventIdBank) == chosenPhnLen_df["phonemeId"]):
            chosenPhnLen_df = createChosenLenPerPhoneme(speechEventIdBank, triphonesBank, feature,
                                                        savePath=chosenLenPerPhonemePath, windoweLen=20e-3,
                                                        stride=5e-3, jobsNum=1)
            chosenPhnLen_df.drop_duplicates(inplace=True)
    else:
        chosenPhnLen_df = createChosenLenPerPhoneme(speechEventIdBank, triphonesBank, feature,
                                                    savePath=chosenLenPerPhonemePath, windoweLen=20e-3,
                                                    stride=5e-3, jobsNum=1)
        chosenPhnLen_df.drop_duplicates(inplace=True)

    # clear duplicate data:
    # chosenPhnLen_df.drop_duplicates(inplace=True)
    chosenPhnLen = int(chosenPhnLen_df["chosenLen"][chosenPhnLen_df["phonemeId"] == str(speechEventIdBank)])

    for fullData_multiPhn in fullData_multiPhn_triphoneList: fullData_multiPhn.chosenPhnLen = chosenPhnLen

    # getting raw feature data by date
    all_dry_data_df_path = Path(saveLocationUrl) / Path(r"data\POC_experiment_all_dry_data_df.csv")
    all_unknown_data_df_path = Path(saveLocationUrl) / Path(r"data\POC_experiment_all_unknown_data_df.csv")
    all_wet_data_df_path = Path(saveLocationUrl) / Path(r"data\POC_experiment_all_wet_data_df.csv")
    dry_data_train_tensor_path = all_dry_data_df_path.parent / Path(r"POC_experiment_all_dry_train_data_tensor.pt")
    dry_data_test_tensor_path = all_dry_data_df_path.parent / Path(r"POC_experiment_all_dry_test_data_tensor.pt")
    all_dry_data_tensor_path = all_dry_data_df_path.parent / Path(r"POC_experiment_all_dry_data_tensor.pt")
    all_unknown_data_tensor_path = all_unknown_data_df_path.parent / Path(r"POC_experiment_all_unknown_data_tensor.pt")
    all_wet_data_tensor_path = all_wet_data_df_path.parent / Path(r"POC_experiment_all_wet_data_tensor.pt")

    def setSaveName(dfName: str):
        if len(speechEventIdBank) > 7: df_dictList_path = saveLocationUrl / Path(dfName + "_" + str(speechEventIdBank[0:7])[0:-1] + ", more_]" + '.pt')
        else: df_dictList_path = saveLocationUrl / Path(dfName + "_" + str(speechEventIdBank) + '.pt')
        return df_dictList_path
    allPaths = [setSaveName(dfName) for dfName in ["all_dry_data_df", "all_unknown_data_df", "all_wet_data_df"]]
    if all([Path.is_file(path) for path in allPaths]):
        # load Data:
        pb = tqdm(range(len(allPaths)), desc='loading preProcessed data, this may take a while...')
        df_dict = {}
        for dfName, loadPath, i in zip(["all_dry_data_df", "all_unknown_data_df", "all_wet_data_df"], allPaths, pb):
            df_dict[dfName] = torch.load(str(loadPath))
        all_dry_data_df, all_unknown_data_df, all_wet_data_df = df_dict["all_dry_data_df"],\
                                                                df_dict["all_unknown_data_df"],\
                                                                df_dict["all_wet_data_df"]
    else:
        df_dictList = Parallel(n_jobs=jobsNum)(delayed(createMultiClinicalInfoDF)
                                               (fullData_multiPhn_triphoneList[i], ["dry", "unknown", "wet"])
                                               for i in tqdm(range(len(fullData_multiPhn_triphoneList)),
                                                             desc="preprocessing data...",
                                                             disable=False))
        Path(allPaths[0]).parent.mkdir(parents=True, exist_ok=True)
        # unpack data into df:
        all_dry_data_df, all_unknown_data_df, all_wet_data_df = unpackDataToDF(df_dictList)
        # save Data:
        pb = tqdm(range(len(allPaths)), desc='saving data, this may take a while...')
        for df, savePath, i in zip([all_dry_data_df, all_unknown_data_df, all_wet_data_df], allPaths, pb):
            torch.save(df, savePath)

    # fix phoneme labeling:
    clinicalInfoDf_list = [all_dry_data_df, all_unknown_data_df, all_wet_data_df]
    pb = tqdm(range(len(clinicalInfoDf_list)), desc='fix phoneme labeling..')
    for CI_df, i in zip(clinicalInfoDf_list, pb):
        triphonID_idx = CI_df.columns.get_loc('triphonID')
        for i in range(len(CI_df)):
            if len(CI_df.iloc[i, triphonID_idx]) == len('[100120502]'):
                CI_df.iloc[i, triphonID_idx] = CI_df.iloc[i, triphonID_idx].replace("[", "")
                CI_df.iloc[i, triphonID_idx] = CI_df.iloc[i, triphonID_idx].replace("]", "")

    # get training tensor data per triphon set:
    fullData_multiPhn_triphoneList = fullData_multiPhn_triphoneList[0].FeatureFrames_df

    train_dry_data_tensor_sets = {}
    for experimentPhnSet, triphonIdSet in zip(experimentPhnSets, experimentPhnIdSets):
        train_dry_data_tensor_sets[str(triphonIdSet)] = getTrainingDataPerTriphonSet(all_dry_data_df,
                                                                                     all_unknown_data_df,
                                                                                     all_wet_data_df, triphonIdSet)

    # get test tensor data per triphon set:
    test_data_dict, test_dt_dict = getTestingDataPerTriphon(all_dry_data_df, all_unknown_data_df, all_wet_data_df,
                                                            triphonesBank, speechEventIdBank, chosenPhnLen,
                                                            featureFrame_size)

    # normalize data:
    if normlizeDataBeforeTraining:
        allDryData_tensor = torch.tensor(all_dry_data_df.drytensor_data)
        allUnknownData_tensor = torch.tensor(all_unknown_data_df.unknowntensor_data)
        allWetData_tensor = torch.tensor(all_wet_data_df.wettensor_data)
        allData = torch.cat((allDryData_tensor, allUnknownData_tensor, allWetData_tensor), 0)
        allDataMean = allData.mean()
        allDataStd = allData.std()
        # normalize train data:
        for triphonIdSet in experimentPhnIdSets:
            train_dry_data_tensor_sets[str(triphonIdSet)] = (train_dry_data_tensor_sets[
                                                                 str(triphonIdSet)] - allDataMean) / allDataStd
        # normalize test data:
        clinicalInfoLabels = ["dry_train", "dry_test", "dry", "unknown", "wet", "all"]
        for speechEventId in speechEventIdBank:
            for ci in clinicalInfoLabels:
                test_data_dict[str(speechEventId)][ci] = (test_data_dict[str(speechEventId)][
                                                              ci] - allDataMean) / allDataStd

    # create train_loader & test_loader for each experiment:
    trianBatch_size = 133
    train_dry_dataLoader_tensor_sets = {}
    for triphonIdSet in experimentPhnIdSets:
        train_dry_dataLoader_tensor_sets[str(triphonIdSet)] = torch.utils.data.DataLoader(
            train_dry_data_tensor_sets[str(triphonIdSet)], batch_size=133, shuffle=shuffle, num_workers=4,
            pin_memory=True)
    test_dataLoader_tensor_sets = {}
    for speechEventId in speechEventIdBank:
        test_dataLoader_tensor_sets[str(speechEventId)] = torch.utils.data.DataLoader(
            test_data_dict[str(speechEventId)]['all'], batch_size=1, shuffle=shuffle, num_workers=4)

    # ----------------------------------------------------------------------------------------------------------------------
    # ----------------------------------------------training----------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------
    save_location_path = Path(saveLocationUrl)
    trainedModelsPerSet = {}
    modelLossPerSet = {}
    experimentPhnTrainingIdSets = [experimentPhnIdSets[0]] # for current run
    experimentPhnTrainingSets = [experimentPhnSets[0]] # for current run
    pbar = tqdm(range(len(experimentPhnTrainingIdSets)), desc='training models')
    for triphonIdSet, i in zip(experimentPhnTrainingIdSets, pbar):
        if len(str(triphonIdSet)) > 50: str_triphonIdSet = str(triphonIdSet[0:7])[0:-1] + ", more_]"
        else: str_triphonIdSet = str(triphonIdSet)

        current_time = datetime.now().strftime("%H:%M:%S")
        pbar.set_description('training model for following SE ' + str_triphonIdSet + ', ' + current_time)
        # setting model save name:
        if normlizeDataBeforeTraining:
            model_name = patient + '_' + str_triphonIdSet + '_optimizer_' + type(
                optimizer).__name__ + '_model_' + type(model).__name__ + '_POC_Experiment_normalized_input'
        else:
            model_name = patient + '_' + str_triphonIdSet + '_optimizer_' + type(
                optimizer).__name__ + '_model_' + type(model).__name__ + '_POC_Experiment'
        if shuffle:
            model_name = model_name + "_shuffleOn"
        else:
            model_name = model_name + "_shuffleOff"
        model_name = model_name + ".pickle"

        trainedModelsPerSet[str(triphonIdSet)], modelLossPerSet[str(triphonIdSet)] = \
            getTrainedModelAndLoss(netModel=AE_Net, AE_Net_manual_weight_init=AE_Net_manual_weight_init, optim=optim,
                                   train_loader=train_dry_dataLoader_tensor_sets[str(triphonIdSet)],
                                   save_location_path=save_location_path, model_name=model_name, epochs=350)

    # pbar.close()

    # creating training loss plots:
    maxLossLen = len(modelLossPerSet[str(experimentPhnTrainingIdSets[0])])
    for triphonIdSet in experimentPhnTrainingIdSets:
        lossLen = len(modelLossPerSet[str(triphonIdSet)])
        if maxLossLen < lossLen: maxLossLen = lossLen
    for triphonIdSet in experimentPhnTrainingIdSets:
        lossLen = len(modelLossPerSet[str(triphonIdSet)])
        if maxLossLen > lossLen:
            modelLossPerSet[str(triphonIdSet)] = modelLossPerSet[str(triphonIdSet)] + [np.nan] * (maxLossLen - lossLen)
    x = list(range(maxLossLen))
    if normlizeDataBeforeTraining:
        title = "MSE training loss\n architecture " + type(model).__name__ + ", learning rate " + str(
            LR) + "\n normalized data\n" + str_triphonesBank
        graph_name = patient + '_' + str_triphonIdSet + '_optimizer_' + type(optim).__name__ + '_model_' + type(
            model).__name__ + '_POC_Experiment_normalized_input_allLosses.png'
    else:
        title = "MSE training loss\n architecture " + type(model).__name__ + ", learning rate " + str(LR) + "\n" + str_triphonesBank
        graph_name = patient + '_' + str_triphonIdSet + '_optimizer_' + type(optim).__name__ + '_model_' + type(
            model).__name__ + '_POC_Experiment_allLosses.png'
    plt.figure(dpi=300)
    plt.title(title)
    plt.xlabel("epocs (each epoc run over all the data)")
    plt.ylabel("loss: MSE score")
    plt.grid()
    for triphonIdSet, phnSet in zip(experimentPhnTrainingIdSets, experimentPhnTrainingSets):
        if len(phnSet) > 7: plotStr_phnSet = str(phnSet[1:7])[0:-1]+' , more_]'
        else:  str(phnSet)
        plt.plot(x, modelLossPerSet[str(triphonIdSet)], label=plotStr_phnSet)
    plt.legend()
    plt.tight_layout()
    saveUrl = str(save_location_path.parent / Path('graphs') / Path(graph_name))
    plt.savefig(saveUrl)

    # ----------------------------------------------------------------------------------------------------------------------
    # ----------------------------------------------testing-----------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    # testing all speech events for each speech events set model:

    # set all test data to the same date axis:
    # minDate = test_dt_dict[str(speechEventIdBank[0])]["all"][0]
    minDate = pd.Timestamp.max
    # maxDate = test_dt_dict[str(speechEventIdBank[0])]["all"][-1]
    maxDate = pd.Timestamp.min
    for speechEventId in speechEventIdBank:
        if not any(test_dt_dict[str(speechEventId)].values()): continue
        for dt in test_dt_dict[str(speechEventId)]["all"]:
            if dt < minDate: minDate = dt
            if dt > maxDate: maxDate = dt
    globalDateTimeVec = pd.date_range(minDate, maxDate)
    globalDateTimeVec = [d.date() for d in globalDateTimeVec]

    allCorrelationScores_df = pd.DataFrame(columns=triphonesBank)
    allVariances_df = pd.DataFrame(columns=triphonesBank)
    for speechEventIdSet, triphoneSet in zip(experimentPhnTrainingIdSets, experimentPhnTrainingSets):  # model/speech events set
        if len(str(triphoneSet)) > 50: str_triphonSetName = str(triphonIdSet[0:6])[0:-1] + ", more_]"
        else: str_triphonSetName = str(triphonIdSet)

        model = trainedModelsPerSet[str(speechEventIdSet)]
        testData_df = getModelTestData_df(speechEventIdBank, triphonesBank, test_dt_dict, model, disablePbar=False)
        new_triphonesBank = testData_df.columns.tolist()[0:-1]
        new_speechEventIdBank = [triphone2speechEventId[phn.split('_')[-1]] for phn in new_triphonesBank]

        # setting data before plotting:
        # ----------------------------
        modelStr = type(model).__name__
        y_title = "MSE test loss"
        if normlizeDataBeforeTraining:
            figureName = patient + '_' + str_triphonSetName + '_optimizer_' + type(optim).__name__ + '_model_' + type(
                model).__name__ + '_POC_Experiment_normalized_allPhnForOneModel.png'
        else:
            figureName = patient + '_' + str_triphonSetName + '_optimizer_' + type(optim).__name__ + '_model_' + type(
                model).__name__ + '_POC_Experiment_allPhnForOneModel.png'
        clinicalInfoNumsScale = [x for x in set(list(testData_df.clinicalInfoNum)) if not np.isnan(x)]
        clinicalInfoNumsScale.sort()
        clinicalInfoScaleLabel = {clinicalInfoNumsScale[0]: "dry_training",
                                  clinicalInfoNumsScale[1]: "dry",
                                  clinicalInfoNumsScale[2]: "unknown",
                                  clinicalInfoNumsScale[3]: "wet"}
        invClinicalInfoScaleLabel = {v: k for k, v in clinicalInfoScaleLabel.items()}
        testPlotSaveUrl = str(Path(saveLocationUrl).parent / Path(r"graphs\\" + figureName))

        # making graphs without wholes (where is nan):
        for triphonLoss in list(testData_df.columns):
            testData_df[triphonLoss] = testData_df[triphonLoss].fillna(method='ffill')
        # get all correlation scores:
        dts = testData_df.index.to_pydatetime().tolist()
        # get correlation:
        allCorrelationScores = {}
        # allCorrelationScores = [getCurollation(patient, dts, testData_df[triphonLoss]) for triphonLoss in list(testData_df.columns)[0:-1]]
        for triphonLoss in list(testData_df.columns)[0:-1]:
            allCorrelationScores[triphonLoss.split("_")[-1]] = getCurollation(patient, dts, testData_df[triphonLoss])
        allCorrelationScores_df = allCorrelationScores_df.append(allCorrelationScores, ignore_index=True)
        # get variance:
        allVariances = {}
        for triphonLoss in list(testData_df.columns)[0:-1]:
            allVariances[triphonLoss.split("_")[-1]] = testData_df[triphonLoss].var()
        allVariances_df = allVariances_df.append(allVariances, ignore_index=True)
        # plotting:
        # --------
        # plot all graphs for each model:
        plotNsave(testData_df, allCorrelationScores, testPlotSaveUrl, invClinicalInfoScaleLabel, show=False)
        # plot single graphs for each model:
        for speechEventId, triphone, col in zip(new_speechEventIdBank, new_triphonesBank, testData_df.columns.tolist()[0:-1]):
            curr_df = testData_df[[col, 'clinicalInfoNum']]
            currTestPlotSaveUrl = Path(testPlotSaveUrl).parent / Path(r"singlePlotForModel") / \
                                  Path("_".join(Path(testPlotSaveUrl).stem.split("_")[
                                                0:-1]) + "_singlePhnForOneModel_" + triphone + ".png")
            plotNsave(curr_df, allCorrelationScores, str(currTestPlotSaveUrl), invClinicalInfoScaleLabel, show=False)
    # plot correlation:
    allCorrelationScores_df["experimentPhnSets"] = experimentPhnTrainingSets
    allCorrelationScores_df = allCorrelationScores_df.set_index("experimentPhnSets")
    f, ax = plt.subplots(figsize=(15, 7.5), dpi=300)
    ax = allCorrelationScores_df.plot(grid=True, ax=ax, linestyle='-')
    allCorrelationScoresGraphPath = Path(testPlotSaveUrl).parent / \
                                    Path("_".join(
                                        Path(testPlotSaveUrl).stem.split("_")[0:-1]) + "_allCorrelationScores.png")
    plt.title("Correlation Scores")
    plt.xticks(rotation=5)
    legendList = allCorrelationScores_df.columns
    if len(legendList) > 7:
        maxKeys = heapq.nlargest(6, allCorrelationScores, key=allCorrelationScores.get)
        legendList = [s + ', curr=' + str(allCorrelationScores[s.split("_")[-1]])[0:7] for s in legendList if
                      s.split("_")[-1] in maxKeys]
    else:
        legendList = [s + ', curr=' + str(allCorrelationScores[s.split("_")[-1]])[0:7] for s in legendList]
    ax.legend(legendList)
    plt.savefig(str(allCorrelationScoresGraphPath))
    plt.show()
    # plot variance:
    allVariances_df["experimentPhnSets"] = experimentPhnTrainingSets
    allVariances_df = allVariances_df.set_index("experimentPhnSets")
    f, ax = plt.subplots(figsize=(15, 7.5), dpi=300)
    ax = allVariances_df.plot(grid=True, ax=ax, linestyle='-')
    allVariancesScoresGraphPath = Path(testPlotSaveUrl).parent / \
                                  Path(
                                      "_".join(Path(testPlotSaveUrl).stem.split("_")[0:-1]) + "_allVariencesScores.png")
    plt.title("Variance Scores")
    plt.xticks(rotation=5)
    legendList = allVariances_df.columns[0:-1]
    if len(legendList) > 7:
        maxKeys = heapq.nlargest(6, allVariances, key=allVariances.get)
        legendList = [s + ', var=' + str(allVariances[s.split("_")[-1]])[0:7] for s in legendList if s.split("_")[-1] in maxKeys]
    else:
        legendList = [s + ', var=' + str(allVariances[s.split("_")[-1]])[0:7] for s in legendList]
    ax.legend(legendList)
    plt.savefig(str(allVariancesScoresGraphPath))
    plt.show()

    print("♪ ┏(°.°)┛┗(°.°)┓ done! ┗(°.°)┛┏(°.°)┓ ♪")

    # TODO: take statistics graphs (mean value and variance)

# fullData.get_all_speech_event_features_for_patient(patient_folder_path_db=dbPath+"\\"+patient[0:3]+"\\"+patient, speech_event=speechEvent)
