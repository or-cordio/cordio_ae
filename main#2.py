import warnings
from multiprocessing.dummy import freeze_support

import torch
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.dates import date2num

from torch import optim, nn, cat
from pathlib import Path
from CordioAutoEncoder import AE_Net, AE_deepNet,  AE_FC_Net, AE_train, AE_test
from CordioAE_preprocessData import preprocessData_singleTriphon
from utils.GeneralTools import Tools

from OrTools import MyTools

# import plotly.express as px

# from Epochsviz.epochsviz import Epochsviz

# warnings.filterwarnings('ignore')
# with warnings.catch_warnings():
warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')
warnings.simplefilter(action='ignore', category=FutureWarning)

# initializing objects:
# --------------------
#  use gpu if available
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")
# create a model from `AE` autoencoder class
# load it to the specified device, either gpu or cpu
model = model = AE_deepNet().to(device)

# create an optimizer object
# Adam optimizer with learning rate 1e-3
LR = 1e-2
optimizer = optim.Adam(model.parameters(), lr=LR)
# mean-squared error loss
criterion = nn.MSELoss()

# getting data ready:
# ------------------
patient = "NHR-0001" #"BLN-0001"
speechEvent = "i"
dbPath = r"\\192.168.55.210\db"
saveLocationUrl = r"\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump"
saveName = patient+"_"+speechEvent+"_mfcc_framesBySe_df"
normlizeDataBeforeTraining = True
jobsNum = 20
forceReCulc = False
forceModelTrain = True # froce training the model and overwrite it in path
feature="mfcc"
# trianBatch_size = 155
if feature == "mfcc": featureFrame_size = 12
else: featureFrame_size = 1


fullData = preprocessData_singleTriphon(patient=patient, speechEvent=speechEvent, dbPath=dbPath, saveLocationUrl=saveLocationUrl,
                                        saveName=saveName, jobsNum=jobsNum, forceReCulc=forceReCulc, feature=feature)
all_dry_data = fullData.FeatureFrames_dry_tensor
all_dry_data_DatesTime = fullData.FeatureFramesDates_dry_List
all_wet_data = fullData.FeatureFrames_wet_tensor
all_wet_data_DatesTime = fullData.FeatureFramesDates_wet_List
all_unknown_data = fullData.FeatureFrames_unknown_tensor
all_unknown_data_DatesTime = fullData.FeatureFramesDates_unknown_List

# rowShaffledFeatureFrames_tensor = fullData.FeatureFrames_tensor[torch.randperm(fullData.FeatureFrames_tensor.size()[0])] # each row describes the features of a single audio file speech event
tmp_df = pd.DataFrame({"DateTime": all_dry_data_DatesTime, "all_dry_data":all_dry_data})
tmp_df = tmp_df.sort_values("DateTime").reset_index()
all_dry_data_DatesTime = list(tmp_df["DateTime"])
all_dry_data = torch.Tensor(tmp_df["all_dry_data"])

# remove irrelevant data data past 31/12/2019:
validDateIdx = [False]*len(all_dry_data_DatesTime)
for i in range(len(all_dry_data_DatesTime)):
    if all_dry_data_DatesTime[i] <= pd.Timestamp(year=2019, month=12, day=31): validDateIdx[i]=True
all_dry_data = all_dry_data[validDateIdx]
all_dry_data_DatesTime = [all_dry_data_DatesTime[i] for i in range(len(all_dry_data_DatesTime)) if validDateIdx[i]]

# normalize data:
if normlizeDataBeforeTraining:
    allData = torch.cat((all_dry_data, all_unknown_data, all_wet_data), 0)
    allDataMean = allData.mean()
    allDataStd = allData.std()
    all_dry_data = (all_dry_data - allDataMean)/allDataStd
    all_unknown_data = (all_unknown_data - allDataMean)/allDataStd
    all_wet_data = (all_wet_data - allDataMean)/allDataStd

# split dry data to train test:
train_dataset = all_dry_data[0:math.floor(len(all_dry_data) * 0.9)].clone()
train_dataset_DatesTime = all_dry_data_DatesTime[0:math.floor(len(all_dry_data_DatesTime) * 0.9)]
test_dry_dataset = all_dry_data[math.ceil(len(all_dry_data) * 0.9):].clone()
test_dry_dataset_DatesTime = all_dry_data_DatesTime[math.ceil(len(all_dry_data_DatesTime) * 0.9):]
test_dataset = cat((train_dataset, test_dry_dataset, all_unknown_data, all_wet_data))
test_dataset_labels = ['dry_training']*len(train_dataset) + \
                      ['dry']*len(test_dry_dataset) + \
                      ['unknown']*len(all_unknown_data) + \
                      ['wet']*len(all_wet_data)
test_dataset_DateTime = train_dataset_DatesTime+test_dry_dataset_DatesTime+all_unknown_data_DatesTime+all_wet_data_DatesTime
# unifying datetime types:
tools = Tools()
test_dataset_DateTime = tools.pdTimestemp2pyDateTime(test_dataset_DateTime)

trianBatch_size = int(train_dataset.size()[1]/featureFrame_size)
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=284, shuffle=True, num_workers=4, pin_memory=True)

test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=4)


# fullData.get_all_speech_event_features_for_patient(patient_folder_path_db=dbPath+"\\"+patient[0:3]+"\\"+patient, speech_event=speechEvent)


if __name__ == '__main__':
    freeze_support()
    # training:
    # --------
    save_location_path = Path(r'\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump')
    if normlizeDataBeforeTraining: model_name = patient+'_trained'+type(model).__name__+'_model_batchIs_18_normalized_data.pickle'
    else: model_name = patient+'_trained'+type(model).__name__+'_model_batchIs_18.pickle'
    mt = MyTools()
    tools = Tools()

    # with warnings.catch_warnings():
    # warnings.filterwarnings('ignore', r'All-NaN axis encountered maxCS = np.nanmax(clinicalStatus_dt) if clinicalStatus_dt else np.nan')
    warnings.filterwarnings('ignore')

    epochs = 350

    if not Path.is_file(save_location_path / model_name) or forceModelTrain:
        model, lossList = AE_train(epochs, train_loader, fullData, featureFrame_size, device, model, optimizer,
                                   criterion, save_location_path=r'\\192.168.55.210\f$\Or-rnd\Autoencoder_project\dump',
                                   modelName=model_name, toleranceParcent=0.05, toleranceLen=4)
        # plot training loss:
        if normlizeDataBeforeTraining:
            title = "MSB training loss\n architecture " + type(model).__name__ + ", learning rate " + str(LR) + "\n normalized data"
        else:
            title = "MSB training loss\n architecture " + type(model).__name__ + ", learning rate " + str(LR)
        saveUrl = str(save_location_path / Path(model_name[:-7])) + "_loss_training_figure.png"

        mt.MyPlot(x=list(range(len(lossList))),
                  y=lossList,
                  title=title,
                  x_labels="epocs (each epoc run over all the data)",
                  y_labels="loss: MSB score",
                  showMin=True,
                  saveURL=saveUrl)
    else:
        model.load_state_dict(torch.load(save_location_path / model_name))

    # testing:
    # -------
    def test(testLoss, test_dataset_DateTime, saveURL=None, y_title="MSE Error", colorCIGraph=False, modelStr=None):
        df = pd.DataFrame({"Date": test_dataset_DateTime, "TestLoss": testLoss})
        for i in range(len(df)):
            df["Date"][i] = df["Date"][i].date()
        # converting clinical info labels to num:
        start = min(testLoss);
        stop = max(testLoss)
        clinicalInfoNums = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
        clinicalInfo2Num = {'dry_training': clinicalInfoNums[0],
                            'dry': clinicalInfoNums[1],
                            'unknown': clinicalInfoNums[2],
                            'wet': clinicalInfoNums[3]}
        test_dataset_labels_num = []
        for test_dataset_label in test_dataset_labels:
            test_dataset_labels_num.append(clinicalInfo2Num[test_dataset_label])
        df["clinicalInfoNum"] = test_dataset_labels_num
        sort_df = df.sort_values('Date')
        df_clean = tools.averageDataInSameDay(sort_df, "Date")

        # setting clinical info of mixed clinical info days into one(maybe bad to do):
        for i in range(len(df_clean)):
            if df_clean["clinicalInfoNum"][i] not in clinicalInfoNums:
                if pd.isnull(df_clean["clinicalInfoNum"][i]):
                    continue
                elif i == 0:
                    df_clean["clinicalInfoNum"][i] = df_clean["clinicalInfoNum"][i + 1]
                else:
                    df_clean["clinicalInfoNum"][i] = df_clean["clinicalInfoNum"][i - 1]

        # scaling clinical info num:
        start = df_clean["TestLoss"].min()
        stop = df_clean["TestLoss"].max()
        clinicalInfoNumsScale = list(tools.frange(start=start, stop=stop, step=(stop - start) / 4))
        clinicalInfoscale = {clinicalInfoNums[0]: clinicalInfoNumsScale[0],
                             clinicalInfoNums[1]: clinicalInfoNumsScale[1],
                             clinicalInfoNums[2]: clinicalInfoNumsScale[2],
                             clinicalInfoNums[3]: clinicalInfoNumsScale[3]}
        clinicalInfoScaleLabel = {clinicalInfoNumsScale[0]: "dry_training",
                                  clinicalInfoNumsScale[1]: "dry",
                                  clinicalInfoNumsScale[2]: "unknown",
                                  clinicalInfoNumsScale[3]: "wet"}
        invClinicalInfoScaleLabel = {v: k for k, v in clinicalInfoScaleLabel.items()}
        for i in range(len(df_clean)):
            if pd.isnull(df_clean["clinicalInfoNum"][i]):
                continue
            else:
                df_clean["clinicalInfoNum"][i] = clinicalInfoscale[df_clean["clinicalInfoNum"][i]]
        # plot:
        f, ax = plt.subplots(figsize=(15,7.5), dpi= 160)
        ax = df_clean.plot(grid=True, ax=ax)
        if colorCIGraph:
            colors = ["green", "limegreen", "grey", "steelblue"]
            for cs, color in zip(list(invClinicalInfoScaleLabel.keys()), colors):
                tmp_df_cs_dates = (df_clean[df_clean["clinicalInfoNum"]==invClinicalInfoScaleLabel[cs]]).index
                # tmp_df_cs_dates = tmp_df_cs_dates.to_pydatetime()
                if list(tmp_df_cs_dates) == []: continue
                csStartDate = tmp_df_cs_dates[0]
                csEndDate = tmp_df_cs_dates[-1]
                plt.axvspan(csStartDate, csEndDate, label=cs, color=color, alpha=0.3)
        ax.legend()
        ax.set_ylabel(y_title)
        # set title:
        if modelStr is None: title = patient+' test'
        else: title = 'Model '+modelStr+': '+patient+' test'
        if "normalized" in saveURL: title = title + "\n normalized data"
        ax.set_title(title, size=18)

        locks = ax.get_yticks()
        locks = list(locks)
        for cs in list(invClinicalInfoScaleLabel.keys()):
            locks.append(invClinicalInfoScaleLabel[cs])
        locks.sort()
        newLabels = []
        for loc in locks:
            if loc in list(clinicalInfoScaleLabel.keys()):
                ap = clinicalInfoScaleLabel[loc]
                newLabels.append(ap + "->       ")
            else:
                newLabels.append("%.1f" % loc)
        # plt.yticks([])
        ax.set_yticks(locks)
        ax.set_yticklabels(newLabels)

        if saveURL is not None: plt.savefig(saveURL+".png")
        plt.show()

    # model.load_state_dict(torch.load(save_location_path / model_name))
    testLoss = AE_test(model, test_loader, fullData, featureFrame_size, device, criterion)

    if normlizeDataBeforeTraining:
        saveURL = str(save_location_path / Path(model_name[:-7])) + "_normalizedtrainData_MSE_test_figure"
    else:
        saveURL = str(save_location_path / Path(model_name[:-7])) + "_MSE_test_figure"
    test(testLoss, test_dataset_DateTime, saveURL=saveURL, y_title="MSE Error")

    criterion = nn.L1Loss()
    testLoss = AE_test(model, test_loader, fullData, featureFrame_size, device, criterion)

    if normlizeDataBeforeTraining:
        saveURL = str(save_location_path / Path(model_name[:-7])) + "_normalizedtrainData_L1_test_figure"
    else:
        saveURL = str(save_location_path / Path(model_name[:-7])) + "_L1_test_figure"
    test(testLoss, test_dataset_DateTime, saveURL=saveURL, y_title="L1 Error")
    # get
    print("done")

    # TODO: take statistics graphs (mean value and variance)
    # TODO: normalize before training








